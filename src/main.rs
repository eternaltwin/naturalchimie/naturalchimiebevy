use bevy::{prelude::*, render::camera::ScalingMode};
use bevy::asset::load_internal_binary_asset;
use bevy::ecs::query::QuerySingleError;

use crate::app_state::{AppState};
use crate::board::BoardPlugin;
use crate::camera_control::PanOrbitCamera;
use crate::common::belt::{Belt, BeltCase};
use crate::common::inventory::Inventory;
use crate::common::player::Player;
use crate::common_ui_sprite::WorldSprite;
use crate::config::Config;
use crate::places::PlacePlugin;
use crate::common::xml_asset::{XmlAsset, XmlAssetLoader};
use crate::menu::MenuPlugin;
use crate::places::model::Align;
use crate::places::places::Places;
use crate::quests::QuestPlugin;
use crate::resume_game::ResumePlugin;
use crate::toml_asset::{DataAssets, TomlAsset, TomlAssetLoader};
use crate::world::WorldPlugin;

mod board;
mod config;
mod app_state;
mod world;
mod toml_asset;
mod common;
mod resume_game;
mod common_ui_sprite;
mod places;
mod camera_control;
mod quests;
mod menu;

/// We will store the world position of the mouse cursor here.
#[derive(Resource, Default)]
struct MouseCoords(Vec2);

fn main() {
    let mut app = App::new();
    app
        .insert_resource(ClearColor(
            Srgba::hex("#181815").unwrap().into(),
        ))
        .add_plugins(
            DefaultPlugins
                .set(ImagePlugin::default_nearest())
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: "Eternalchimie".into(),
                        resolution: (100., 100.).into(),
                        resizable: false,
                        ..default()
                    }),
                    ..default()
                })
                .build(),
        );
    load_internal_binary_asset!(
        app,
        TextStyle::default().font,
        "../assets/fonts/LondonTwo.ttf",
        |bytes: &[u8], _path: String| { Font::try_from_bytes(bytes.to_vec()).unwrap() }
    );

    app
        .init_asset::<TomlAsset>()
        .init_asset_loader::<TomlAssetLoader>()
        .init_asset::<XmlAsset>()
        .init_asset_loader::<XmlAssetLoader>()
        .insert_resource(Player { id: 0, first_play_done: false, active_quest: None, quest_status: Default::default(), school: Align::None, reput: Default::default(), effects: vec![], public_alias: "Pseudo".to_string(), level: 0, location: "grenie".to_string() })
        .insert_resource(Inventory { kubor:0,  elements: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], items: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] })
        .insert_resource(Belt { cases: [BeltCase::Blocked, BeltCase::Blocked, BeltCase::Blocked, BeltCase::Blocked] })
        .add_plugins(WorldPlugin)
        .add_plugins(MenuPlugin)
        .add_plugins(BoardPlugin)
        .add_plugins(ResumePlugin)
        .add_plugins(PlacePlugin)
        .add_plugins(QuestPlugin)
        .add_systems(Startup, setup)
        .add_systems(Update, (apply_config,cursor_system))
        .add_systems(Update, (redraw_state).run_if(in_state(AppState::RedrawWorld)))
        .init_state::<AppState>()
        .run();
}

fn apply_config(
    mut commands: Commands,
    toml_assets: Res<Assets<TomlAsset>>,
    mut data_assets: ResMut<DataAssets>,
    mut window_query: Query<&mut Window>,
) {
    let config_asset = toml_assets.get(&data_assets.handle_config);
    if data_assets.config_updated || config_asset.is_none() {
        return;
    }
    info!("Custom asset loaded: {:?}", config_asset.unwrap());
    let config = Config::extract_toml(&config_asset.unwrap().0);

    let mut window = window_query.get_single_mut().unwrap();
    window.resolution.set(config.view.x, config.view.y);

    let mut camera = Camera2dBundle::default();

    camera.projection.scaling_mode = ScalingMode::AutoMin {
        min_width: config.view.x.clone(),
        min_height: config.view.y.clone(),
    };

    commands.spawn((camera, PanOrbitCamera::default()));
    commands.insert_resource(config);
    data_assets.config_updated = true;
    info!("config down")
}


fn cursor_system(
    mut coords: ResMut<MouseCoords>,
    q_window: Query<&Window>,
    q_camera: Query<(&Camera, &GlobalTransform), With<PanOrbitCamera>>,
) {
    match  q_camera.get_single() {
        Ok((camera, camera_transform)) => {
            let window = q_window.single();

            // check if the cursor is inside the window and get its position
            // then, ask bevy to convert into world coordinates, and truncate to discard Z
            if let Some(world_position) = window.cursor_position()
                .and_then(|cursor| camera.viewport_to_world(camera_transform, cursor))
                .map(|ray| ray.origin.truncate())
            {
                coords.0 = world_position;
            }
        }
        _ => {}
    }
}


fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    let config_data: Handle<TomlAsset> = asset_server.load("config.toml");
    commands.insert_resource(DataAssets { handle_config: config_data, config_updated: false });
    commands.insert_resource(WorldSprite::new(&asset_server));
    commands.insert_resource(MouseCoords::default());
    info!("setup down")
}

fn redraw_state(mut next_app_state: ResMut<NextState<AppState>>) {
    println!("redraw world like in alice in the wonderworld :D");
     next_app_state.set(AppState::World);
}