use bevy::prelude::*;
use crate::board::element::{ElementEssence};
use crate::board::position::{BoardPosition};
use crate::board::sprite::SpriteSheets;


#[derive(Resource, Debug, Default)]
pub struct Rotor {
    which : usize, // index of current position in the array below.
    doubled_position : [BoardPosition; 2], //first element is rotor position if vertical, second horizontal
                                           //By doubling coordinates, the rotor can stand between two elements
}

#[derive(Component, Debug, Default)]
pub struct NewElement {
    pub kind: ElementEssence,
    pub relative_position : BoardPosition, //relative to the rotor.
}

impl Rotor {
    fn default() ->Rotor {
        Rotor { which: 0, doubled_position: [BoardPosition{x:5,y:16},BoardPosition{x:4, y:17}]}
    }
    fn get_doubled_x(&self) -> i8{
        self.doubled_position[self.which].x
    }
    fn get_doubled_y(&self) -> i8{
        self.doubled_position[self.which].y
    }

    pub fn shift(&mut self, elts : Vec<Mut<NewElement>>){
        for mut e in elts{
            e.relative_position = BoardPosition{x: -e.relative_position.y,  y:e.relative_position.x};
        }
        self.which = 1-self.which;
    }
    pub fn move_right(&mut self, max_x: i8){
        if self.doubled_position[1].x <= 2*max_x-1{
            self.doubled_position[1].x += 2;
        }
        if self.doubled_position[0].x + 2 <= 2*max_x{
            self.doubled_position[0].x += 2;
        }
    }
    pub fn move_left(&mut self, max_x: i8){
        if self.doubled_position[1].x >= 2{
            self.doubled_position[1].x -= 2;
        }
        if self.doubled_position[0].x > 2{
            self.doubled_position[0].x -= 2;
        }
    }
    pub fn reset(&mut self){
        self.which = 0;
        self.doubled_position[0]= BoardPosition{x:5, y:16};
        self.doubled_position[1] = BoardPosition{x:4, y:17};
    }
    pub fn element_position(&self, relative : BoardPosition) ->BoardPosition{
        BoardPosition{x: (self.get_doubled_x()+relative.x)/2, y:(self.get_doubled_y()+relative.y)/2}
    }
}

impl NewElement {

    fn new(relative_position: BoardPosition, r :&Rotor, max_element: i32) -> NewElement {
        let essence = ElementEssence::from_random(r.element_position(relative_position), max_element);
        NewElement {kind: essence, relative_position: relative_position}
    }

    pub fn spawn(parent: &mut  ChildBuilder, assets: &Assets<Image>, sprite_sheet: &SpriteSheets, spirit_size: f32, r : &mut Rotor, max_element: i32) {
        r.reset();
        let a = NewElement::new(BoardPosition { x: -1, y: 0}, r, max_element);
        let b = NewElement::new(BoardPosition { x: 1, y: 0}, r, max_element);
        parent.spawn((a.kind.get_sprite_bundle(assets, sprite_sheet, spirit_size), a));
        parent.spawn((b.kind.get_sprite_bundle(assets, sprite_sheet, spirit_size), b));
    }

    // returns an ElementEssence with the right position. Please put it in the board and make it fall.
    pub fn detach(&mut self, r : &Rotor) -> ElementEssence { 
        self.update_position(r);
        self.kind.clone()
    }

    pub fn update_position(&mut self, r : &Rotor){
        self.kind.position = r.element_position(self.relative_position);
    }

    //should disappear
    pub fn fall(&mut self, y: i8 ) -> i8 {
        y+(self.relative_position.y+1)/2
    }
}