use bevy::prelude::*;
use crate::board::sprite::{ScoreDisplay};
use crate::common::element::Element;


#[derive(Resource, Default)]
pub struct Score {
    from_element: u32,
    from_merge: u32,
    combo_length: u32,
    pub max_element: i32,
}

pub fn refresh_scoreboard(score: Res<Score>, mut query_scores: Query<&mut Text, With<ScoreDisplay>>) {
    match query_scores.get_single_mut() {
        Ok(mut text) => { text.sections[0].value = score.get().to_string() }
        Err(e) => {
            info!("{:?}", e)
        }
    };
}


impl Score {
    pub fn new() -> Score {
        Score { from_element: 0, from_merge: 0, combo_length: 0, max_element: 3 }
    }

    pub fn merge(&mut self, el: &Element, count: u32) {
        let element_rank = el.as_index() as i32;
        println!("element_rank {} >= self.max_element {} ? {}", self.max_element, self.max_element, !(element_rank < self.max_element));
        if !(element_rank < self.max_element) {
            self.max_element = element_rank + 1;
        }
        println!("el rank: {}, max in score {}", self.max_element, self.max_element);
        self.from_element = self.from_element - (count * get_element_score(el)) + get_element_score(&el.next());
    }

    pub fn add_bonus_combo(&mut self) {
        self.from_merge = self.from_merge + (10 * self.combo_length );
        self.combo_length += 1;
    }

    pub fn reset_combo(&mut self) {
        self.combo_length = 0;
    }

    pub fn add_element(&mut self, element: &Element) {
        self.from_element = self.from_element + get_element_score(element);
    }

    pub fn get(&self) -> u32 {
        self.from_merge + self.from_element
    }
}

fn get_element_score(element: &Element) -> u32 {
    match element {
        Element::GreenPotion => { 1 }
        Element::YellowPotion => { 3 }
        Element::RedPotion => { 9 }
        Element::PurplePotion => { 27 }
        Element::MintLeaf => { 81 }
        Element::MadEye => { 243 }
        Element::Cranos => { 729 }
        Element::Flaum => { 2187 }
        Element::Oxide => { 6561 }
        Element::Copper => { 19683 }
        Element::Mercury => { 59049 }
        Element::GoldNugget => { 177147 }
    }
}