use bevy::prelude::*;
use bevy::reflect::erased_serde::__private::serde;
use bevy::render::render_resource::Extent3d;
use bevy::sprite::Anchor;
use rand::Rng;
use serde::{Deserialize, Serialize};
use crate::board::position::BoardPosition;
use crate::board::sprite::SpriteSheets;
use crate::common::element::Element;

const Y_OVERFLOW: f32 = 5.;
const ELEMENT_WEIGHT_CUMULATED: [i32;13] = [0,18,36,54,72,84,92,99,104,108,109,110,110];

#[derive(Debug, PartialEq, Default, Reflect, Serialize, Deserialize)]
#[reflect()]
pub enum Direction {
    #[default]
    Right,
    Bottom,
    Left,
    Top,
}


#[derive(Component, Copy, Clone, PartialEq, Debug, Default, Serialize, Deserialize)]
pub struct ElementEssence {
    pub kind: Element,
    pub position: BoardPosition,
    pub updated: bool,
}

impl ElementEssence {
    pub fn from_random(position: BoardPosition, max_element: i32) -> ElementEssence {
        let mut rng = rand::thread_rng();
        let max = ELEMENT_WEIGHT_CUMULATED[max_element as usize];
        let dice = rng.gen_range(1..=max);
        let index= ELEMENT_WEIGHT_CUMULATED.iter().enumerate().find(|(_idx,x)|{
            println!("test {} > {}, {}", **x, dice, **x<dice);
            **x > dice
        } ).unwrap_or(((max_element+1) as usize, &0)).0;
        println!("max {:?}, array {:?}, random {:?}, found {:?}",max_element, ELEMENT_WEIGHT_CUMULATED, dice, index);
        ElementEssence::from_i32(index as i32 -1, position)
    }

    pub fn from_i32(value: i32, position: BoardPosition) -> ElementEssence {
        ElementEssence { position, kind: Element::from_i32(value), updated: true  }
    }

    pub fn get_sprite_bundle(&self, assets: &Assets<Image>, sprite_sheet: &SpriteSheets, spirit_size: f32) -> SpriteBundle {
        let real_x = self.get_real_x(spirit_size);
        let real_y = self.get_real_y(spirit_size);
        let texture_handle = self.kind.get_texture(&sprite_sheet.items);
        let image = assets.get(texture_handle);
        let size = image
            .and_then(|image| Some(image.texture_descriptor.size))
            .unwrap_or(Extent3d { height: 48, width: 48, depth_or_array_layers: 0 });
        SpriteBundle {
            transform: Transform {
                translation: Vec3::new(real_x, real_y, 0.8 - real_y / 1000.),
                scale: Vec3::new(spirit_size / size.width as f32, spirit_size / size.height as f32, 1.0),
                ..Default::default()
            },
            texture: texture_handle.clone(),
            sprite: Sprite {
                anchor: Anchor::BottomLeft,
                ..Default::default()
            },
            ..Default::default()
        }
    }

    pub fn get_real_x(&self, spirit_size: f32) -> f32 {
        (self.position.x as f32) * spirit_size
    }

    pub fn get_real_y(&self, spirit_size: f32) -> f32 {
        (self.position.y as f32) * (spirit_size - Y_OVERFLOW)
    }
}
