use crate::board::board::Board;
use crate::board::position::BoardPosition;
use crate::board::score::Score;
use crate::common::element::Element;

#[derive(Debug)]
enum MergeMarker {
    Spirit,
    Invalid,
}

pub fn apply_merge_on_board(board: &mut Board, cases: Vec<BoardPosition>, score: &mut Score) {
    let mut acc: Vec<(Element, Vec<Vec<Option<MergeMarker>>>)> = Vec::new();

    println!("cases: {:?}", cases);
    cases.into_iter()
        .for_each(|case| {
            match board.grid[case.x as usize][case.y as usize] {
                None => {}
                Some(kind) => {
                    let mut merge_markers: Vec<Vec<Option<MergeMarker>>> = Vec::new();
                    for x in 0..board.grid.len() {
                        merge_markers.push(Vec::new());
                        for _ in 0..board.grid[x].len() {
                            merge_markers[x].push(None);
                        }
                    }
                    check_case_exists_with_kind(&board, &mut merge_markers, case.x as usize, case.y as usize, kind);
                    acc.push((kind, merge_markers));
                }
            }
        });

    acc
        .iter()
        .for_each(|(kind, grid)| {
            let mut delete_vec: Vec<BoardPosition> = Vec::new();

            for y in 0..board.height {
                for x in 0..board.grid.len() {
                    let case = &grid[x][y];
                    match case {
                        Some(MergeMarker::Spirit) => {
                            let position = BoardPosition { x: x as i8, y: y as i8 };
                            delete_vec.push(position);
                        }
                        _ => {}
                    }
                }
            }
            dispatch_add_and_remove(board, score, kind, delete_vec);
        });

    board.print();
}

fn dispatch_add_and_remove(board: &mut Board, score: &mut Score, kind: &Element, mut delete_vec: Vec<BoardPosition>) {
    if delete_vec.len() > 2 {
        let case = delete_vec[0].clone();
        //avoid add two element for on chain of same element
        if !board.add.iter().any(|(pos, _)| { pos.x == case.x && pos.y == case.y }) {
            score.merge(kind, delete_vec.len() as u32);
            board.add.push((case, kind.next()));
            delete_vec.into_iter().for_each(|position| {
                board.remove.push(position);
            });
        }
    }
}

fn check_case_exists_with_kind(board: &Board, markers: &mut Vec<Vec<Option<MergeMarker>>>, x: usize, y: usize, kind: Element) {
    let test = board.grid.len() > x && board.grid[x].len() > y;
    if test {
        match markers[x][y] {
            None => {
                check_case_kind_and_explore_board(board, markers, x, y, kind);
            }
            Some(_) => {}
        }
    }
}

fn check_case_kind_and_explore_board(board: &Board, markers: &mut Vec<Vec<Option<MergeMarker>>>, x: usize, y: usize, kind: Element) {
    if board.grid[x][y] == Some(kind) {
        markers[x][y] = Some(MergeMarker::Spirit);
        check_case_exists_with_kind(board, markers, x + 1, y, kind);
        if x > 0 { check_case_exists_with_kind(board, markers, x - 1, y, kind); }
        check_case_exists_with_kind(board, markers, x, y + 1, kind);
        if y > 0 { check_case_exists_with_kind(board, markers, x, y - 1, kind); }
    } else {
        markers[x][y] = Some(MergeMarker::Invalid);
    }
}


#[cfg(test)]
mod test {
    use crate::board::board::Board;
    use crate::board::position::BoardPosition;
    use crate::common::element::Element;
    use crate::board::merge_algo::apply_merge_on_board;
    use crate::board::score::Score;

    fn provide_l_board() -> Board {
        let mut board = Board::new(5);
        board.grid[3][0] = Some(Element::GreenPotion);
        board.grid[3][1] = Some(Element::GreenPotion);
        board.grid[3][2] = Some(Element::GreenPotion);
        board.grid[4][0] = Some(Element::GreenPotion);
        board
    }

    #[test]
    fn apply_merge_on_board_should_have_stack_ordered_bottom_to_top() {
        let cases = vec![BoardPosition { x: 3, y: 2 }, BoardPosition { x: 4, y: 0 }];
        let mut board = provide_l_board();
        let mut score = Score::new();
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        apply_merge_on_board(&mut board, cases, &mut score);

        let remove_expected = vec![
            BoardPosition { x: 3, y: 0 },
            BoardPosition { x: 4, y: 0 },
            BoardPosition { x: 3, y: 1 },
            BoardPosition { x: 3, y: 2 },
        ];

        assert_eq!(board.add, vec![(BoardPosition { x: 3, y: 0 }, Element::YellowPotion)]);
        assert_eq!(board.remove, remove_expected);
    }

    #[test]
    fn apply_merge_on_board_should_not_merge_if_gap_between_new_cases() {
        let cases = vec![BoardPosition { x: 2, y: 0 }, BoardPosition { x: 3, y: 1 }];
        let mut score = Score::new();
        let mut board = Board::new(5);
        board.grid[2][0] = Some(Element::YellowPotion);
        board.grid[3][0] = Some(Element::GreenPotion);
        board.grid[3][1] = Some(Element::GreenPotion);
        board.grid[3][2] = Some(Element::RedPotion);
        board.grid[3][3] = Some(Element::YellowPotion);
        board.grid[3][4] = Some(Element::YellowPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::RedPotion);
        apply_merge_on_board(&mut board, cases, &mut score);

        assert_eq!(board.add, vec![]);
        assert_eq!(board.remove, vec![]);
    }


    #[test]
    fn apply_merge_on_board_should_merge_correctly_in_hight() {
        let cases = vec![BoardPosition { x: 2, y: 0 }, BoardPosition { x: 3, y: 4 }];
        let mut board = Board::new(5);
        board.grid[2][0] = Some(Element::RedPotion);
        board.grid[3][0] = Some(Element::RedPotion);
        board.grid[3][1] = Some(Element::YellowPotion);
        board.grid[3][2] = Some(Element::YellowPotion);
        board.grid[3][3] = Some(Element::YellowPotion);
        board.grid[3][4] = Some(Element::YellowPotion);
        let mut score = Score::new();
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::RedPotion);
        score.add_element(&Element::RedPotion);
        apply_merge_on_board(&mut board, cases, &mut score);

        let remove_expected = vec![
            BoardPosition { x: 3, y: 1 },
            BoardPosition { x: 3, y: 2 },
            BoardPosition { x: 3, y: 3 },
            BoardPosition { x: 3, y: 4 },
        ];


        assert_eq!(board.add, vec![(BoardPosition { x: 3, y: 1 }, Element::RedPotion)]);
        assert_eq!(board.remove, remove_expected);
    }

    #[test]
    fn apply_merge_on_board_should_merge_correctly_in_bottom_right() {
        let cases = vec![BoardPosition { x: 1, y: 3 }, BoardPosition { x: 2, y: 1 }];
        let mut board = Board::new(6);
        board.grid[0][0] = Some(Element::RedPotion);
        board.grid[1][0] = Some(Element::YellowPotion);
        board.grid[1][1] = Some(Element::GreenPotion);
        board.grid[1][2] = Some(Element::GreenPotion);
        board.grid[1][3] = Some(Element::YellowPotion);
        board.grid[2][0] = Some(Element::YellowPotion);
        board.grid[2][1] = Some(Element::YellowPotion);
        let mut score = Score::new();
        score.add_element(&Element::RedPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::YellowPotion);
        score.add_element(&Element::YellowPotion);
        apply_merge_on_board(&mut board, cases, &mut score);

        let remove_expected = vec![
            BoardPosition { x: 1, y: 0 },
            BoardPosition { x: 2, y: 0 },
            BoardPosition { x: 2, y: 1 },
        ];


        assert_eq!(board.add, vec![(BoardPosition { x: 1, y: 0 }, Element::RedPotion)]);
        assert_eq!(board.remove, remove_expected);
    }

    #[test]
    fn apply_merge_on_board_should_remove_all_potion_in_chain() {
        let cases = vec![BoardPosition { x: 3, y: 1 }, BoardPosition { x: 4, y: 1 }];
        let mut board = Board::new(6);
        board.grid[2][0] = Some(Element::MintLeaf);
        board.grid[2][1] = Some(Element::GreenPotion);
        board.grid[2][2] = Some(Element::YellowPotion);
        board.grid[2][3] = Some(Element::GreenPotion);
        board.grid[2][4] = Some(Element::GreenPotion);
        board.grid[3][0] = Some(Element::PurplePotion);
        board.grid[3][1] = Some(Element::GreenPotion);
        board.grid[4][0] = Some(Element::GreenPotion);
        board.grid[4][1] = Some(Element::GreenPotion);
        let mut score = Score::new();
        score.add_element(&Element::MintLeaf);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::GreenPotion);
        score.add_element(&Element::PurplePotion);
        apply_merge_on_board(&mut board, cases, &mut score);

        let remove_expected = vec![
            BoardPosition { x: 4, y: 0 },
            BoardPosition { x: 2, y: 1 },
            BoardPosition { x: 3, y: 1 },
            BoardPosition { x: 4, y: 1 },
        ];


        assert_eq!(board.add, vec![(BoardPosition { x: 4, y: 0 }, Element::YellowPotion)]);
        assert_eq!(board.remove, remove_expected);
    }
}