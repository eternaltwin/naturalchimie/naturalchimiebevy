use bevy::prelude::{Resource, States};
use crate::board::position::BoardPosition;
use crate::common::element::Element;

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
pub enum BoardState {
    #[default]
    Loading,
    ResizeBoardRoot,
    WaitPlayer,
    SpawnNewElements,
    entitiesModifications,
    Gravity,
    Merge,
    Pause,
    CheckEndGame,
    ListenUser,
}


#[derive(Resource, Default)]
pub struct Board {
    pub grid: Vec<Vec<Option<Element>>>,
    pub height: usize,
    pub add: Vec<(BoardPosition, Element)>,
    pub remove: Vec<BoardPosition>
}

impl Board {
    pub fn new(size: i32) -> Board {
        let mut board = Board {
            grid: Vec::new(),
            height: 10,
            add: Vec::new(),
            remove: Vec::new(),
        };
        for _ in 0..size {
            let mut vec = Vec::new();
            for _ in 0..board.height {
                vec.push(None);
            }
            board.grid.push(vec);
        }
        board
    }

    pub fn print(&self) {
        for y in (0..self.height).rev() {
            print!("{} | ", y);
            for x in 0..self.grid.len() {
                let s = match self.grid[x][y] {
                    None => {"."}
                    Some(Element::GreenPotion) => {"A"}
                    Some(Element::YellowPotion) => {"B"}
                    Some(Element::RedPotion) => {"C"}
                    Some(Element::PurplePotion) => {"D"}
                    Some(_) => {"+"}
                };
                print!("{}  | ", s);
            }
            println!("");
        }
        println!("add: {:?}", self.add);
        println!("remove: {:?}", self.remove);
    }

    pub fn clear(&mut self) {
        for x in 0..self.grid.len() {
            for y in 0..self.grid[x].len() {
                self.grid[x][y] = None
            }
        }
    }
}