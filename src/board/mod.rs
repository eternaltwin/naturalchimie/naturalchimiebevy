use std::cmp::PartialEq;
use bevy::prelude::*;
use bevy::sprite::Anchor;
use bevy::utils::HashMap;
use crate::board::rotor::{NewElement, Rotor};
use crate::board::position::{BoardPosition};
use crate::board::element::{ElementEssence};
use crate::config::Config;
use crate::app_state::{AppState, Loot};
use crate::board::board::{Board, BoardState};
use crate::board::input::{apply_input_command, react_to_user_keyboard_input, InputCommands};
use crate::board::score::{refresh_scoreboard, Score};
use crate::board::merge_algo::apply_merge_on_board;
use crate::board::sprite::{BoardEnumUi, GridDisplay, SpriteSheets};
use crate::common::element::Element;

mod element;
mod input;
mod rotor;
mod position;
mod score;
mod board;
mod merge_algo;
mod sprite;

#[derive(Component)]
pub struct RootBoard;

pub struct BoardPlugin;

impl Plugin for BoardPlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<BoardState>();
        app.add_systems(OnEnter(AppState::Game), setup_board);
        app.add_systems(Update, (resize_board_root).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::ResizeBoardRoot)));
        app.add_systems(Update, (loading).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::Loading)));
        app.add_systems(Update, (spawn_new_elements).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::SpawnNewElements)));
        app.add_systems(Update, (react_to_user_keyboard_input).run_if(in_state(AppState::Game)));
        app.add_systems(Update, (apply_input_command, refresh_grid).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::ListenUser)));
        app.add_systems(Update, (refresh_scoreboard, compute_merge, refresh_scoreboard).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::Merge)));
        app.add_systems(Update, (apply_gravity).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::Gravity)));
        app.add_systems(Update, (entities_modifications).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::entitiesModifications)));
        app.add_systems(Update, (check_if_game_continue).run_if(in_state(AppState::Game)).run_if(in_state(BoardState::CheckEndGame)));
        app.add_systems(OnExit(AppState::Game), cleanup);
    }
}

fn setup_board(mut commands: Commands, asset_server: Res<AssetServer>, mut board_state: ResMut<NextState<BoardState>>) {
    let board = Board::new(6);
    let rotor = Rotor::default();
    println!("enter board");
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Srgba::hex("#23282e").unwrap().into(),
                custom_size: Some(Vec2::new(200., 355.)),
                anchor: Anchor::BottomLeft,
                ..Default::default()
            },
            ..Default::default()
        },
        RootBoard,
        BoardEnumUi
    ));
    commands.insert_resource(InputCommands::new());
    commands.insert_resource(Score::new());
    commands.insert_resource(SpriteSheets::new(&asset_server));
    commands.insert_resource(board);
    commands.insert_resource(rotor);
    board_state.set(BoardState::Loading);
}


fn cleanup(
    mut commands: Commands,
    ui_items: Query<Entity, With<BoardEnumUi>>,
)
{
    println!("exit board");
    commands.remove_resource::<Board>();
    commands.remove_resource::<Score>();
    for e in ui_items.iter() {
        commands.entity(e).despawn_recursive();
    }
}

fn apply_gravity(
    mut board: ResMut<Board>,
    mut cases: Query<(&mut ElementEssence, &mut Transform)>,
    mut board_state: ResMut<NextState<BoardState>>,
    config: Res<Config>,
)
{
    let mut move_occur = false;
    let start_index = board
        .grid
        .iter()
        .map(|column| {
            column
                .iter()
                .enumerate()
                .find(|(_idx, el)| { el.is_none() })
                .and_then(|(idx, _)| Some(idx))
                .unwrap_or(column.len()) as i8
        })
        .collect::<Vec<i8>>();
    board.clear();

    for (mut case, mut transform) in &mut cases {
        if start_index[case.position.x as usize] < case.position.y {
            move_occur = true;
            case.position.y = case.position.y - 1;
            transform.translation.y = case.get_real_y(config.board.spirit_size);
            case.updated = true;
        }
        board.grid[case.position.x as usize][case.position.y as usize] = Some(case.kind.clone());
    }
    info!("move occur ? {}", move_occur);
    if !move_occur {
        board_state.set(BoardState::Merge);
    }
}

fn entities_modifications(
    mut commands: Commands,
    mut board: ResMut<Board>,
    cases: Query<(Entity, &mut ElementEssence)>,
    mut board_state: ResMut<NextState<BoardState>>,
    board_root: Query<Entity, With<RootBoard>>,
    assets: Res<Assets<Image>>,
    sprite_sheet: Res<SpriteSheets>,
    config: Res<Config>,
)
{
    for (entity, case) in cases.iter() {
        if board.remove.iter().any(|e| e == &case.position) {
            board.grid[case.position.x as usize][case.position.y as usize] = None;
            commands.entity(entity).despawn();
        }
    }
    let entity_board = board_root.get_single().expect("Somewith go wrong at board creation");
    for (position, element) in board.add.clone() {
        create_element(&mut commands, &mut board, &entity_board, &assets, &sprite_sheet, &config, position, element);
    }
    board.remove = Vec::new();
    board.add = Vec::new();

    board_state.set(BoardState::Gravity);
}

fn create_element(commands: &mut Commands, board: &mut Board, entity_root: &Entity, assets: &Assets<Image>, sprite_sheet: &SpriteSheets, config: &Config, position: BoardPosition, element: Element) {
    board.grid[position.x as usize][position.y as usize] = Some(element.clone());
    println!("create element at {} {}", position.x, position.y);
    let spirit = ElementEssence { kind: element.clone(), position, updated: true };
    commands
        .entity(*entity_root)
        .with_children(|parent| {
            parent
                .spawn((spirit.get_sprite_bundle(&assets, &sprite_sheet, config.board.spirit_size), spirit));
        });
}

pub fn compute_merge(
    mut board_state: ResMut<NextState<BoardState>>,
    mut board: ResMut<Board>,
    mut score: ResMut<Score>,
    mut cases: Query<(Entity, &mut ElementEssence)>,
)
{
    let positions = cases.iter()
        .filter(|(_, el)| el.updated)
        .map(|(_, el)| {
            el.position.clone()
        })
        .collect();
    cases.iter_mut()
        .for_each(|(_, mut el)| {
            el.updated = false;
        });
    apply_merge_on_board(&mut board, positions, &mut score);
    if board.remove.is_empty() && board.add.is_empty() {
        //if no merge are found
        board_state.set(BoardState::CheckEndGame);
    } else {
        score.add_bonus_combo();
        //replace the old merge with the new cases
        board_state.set(BoardState::entitiesModifications);
    }
}

fn check_if_game_continue(
    mut commands: Commands,
    mut board_state: ResMut<NextState<BoardState>>,
    mut app_state: ResMut<NextState<AppState>>,
    board: ResMut<Board>,
    score: ResMut<Score>,
)
{
    match board.grid.iter().position(|column| column[7].is_some()) {
        Some(_) => {
            let mut count_by_elements = HashMap::new();
            let mut max_available = 0;
            for column in &board.grid {
                for case in column {
                    match case {
                        Some(el) => {
                            count_by_elements
                                .entry(*el)
                                .and_modify(|count| { *count += 1 })
                                .or_insert(1);
                            max_available = max_available + 1;
                        }
                        None => {}
                    }
                }
            };
            let mut elements: HashMap<Element, u8> = HashMap::new();
            let mut number_of_gettable_elements = 6;
            let mut i = 0;
            while number_of_gettable_elements > 0 && i < number_of_gettable_elements {
                i = i + 1;
                let mut ordered_counted_elements = count_by_elements.iter().map(|(a, c)| (a.clone(), c.clone())).collect::<Vec<(Element, u8)>>();
                ordered_counted_elements.sort_by(|(a, _), (b, _)| b.as_index().cmp(&a.as_index()));
                println!("count_by_elements {:?}", ordered_counted_elements);

                for (element, available) in ordered_counted_elements {
                    println!("get {:?}, max {:?}", element, available);
                    if number_of_gettable_elements > 0 && i <= available {
                        elements
                            .entry(element)
                            .and_modify(|count| { *count += 1 })
                            .or_insert(1);
                        number_of_gettable_elements = number_of_gettable_elements - 1;
                    }
                }
            }
            commands.insert_resource(Loot { score: score.get(), exp: 0, kubor: 26, elements });
            app_state.set(AppState::ResumeGame);
        }
        None => {
            board_state.set(BoardState::SpawnNewElements);
            println!("et le jeu continue");
        }
    }
}

fn spawn_new_elements(
    mut commands: Commands,
    mut board_state: ResMut<NextState<BoardState>>,
    mut score: ResMut<Score>,
    mut rotor: ResMut<Rotor>,
    config: Res<Config>,
    assets: Res<Assets<Image>>,
    sprite_sheet: Res<SpriteSheets>,
    board_root: Query<Entity, With<RootBoard>>,
)
{
    score.reset_combo();
    let entity_board = board_root.get_single().expect("Somewith go wrong at board creation of rotor content");
    commands
        .entity(entity_board)
        .with_children(|parent| {
            NewElement::spawn(parent, &assets, &sprite_sheet, config.board.spirit_size, &mut *rotor, score.max_element);
        });
    board_state.set(BoardState::ListenUser);
}


fn resize_board_root(
    ui_board: Query<Entity, With<GridDisplay>>,
    mut transform_params: ParamSet<(
        TransformHelper,
        Query<&mut Transform, With<RootBoard>>,
    )>,
    mut board_state: ResMut<NextState<BoardState>>,
    config: Res<Config>,
)
{
    let ui_entity = ui_board.get_single().expect("no board pos found");
    let Ok(global) = transform_params.p0().compute_global_transform(ui_entity) else {
        return;
    };
    let mut translation = global.translation();
    // the default behavior is to compute translation from Anchor in Anchor::Center
    // We want to do that from Anchor::BottomLeft. So we remove half of height(355) and width (200).
    // We need also to count the size of foreground
    translation.x = (config.view.x / 2.) - translation.x - 90.;
    translation.y = (config.view.y / 2.) - translation.y - 177.;
    transform_params.p1().single_mut().translation = translation;
    board_state.set(BoardState::SpawnNewElements); // must be changed to wait player to display information on how play
}

fn loading(mut commands: Commands, assets: Res<Assets<Image>>, sprites: Res<SpriteSheets>, mut board_state: ResMut<NextState<BoardState>>) {
    let handles = sprites.get_all_handles();
    let loaded = handles.iter().fold(0, |mut acc, handle| {
        if assets.get(handle).is_some() {
            acc = acc + 1;
        }
        acc
    });
    let loading = loaded * 100 / handles.len();
    if loading == 100 {
        sprites.load_board_ui(&mut commands, [
            Element::GreenPotion,
            Element::YellowPotion,
            Element::RedPotion,
            Element::PurplePotion,
            Element::MintLeaf,
            Element::MadEye,
            Element::Cranos,
            Element::Flaum,
            Element::Oxide,
            Element::Copper,
            Element::Mercury,
            Element::GoldNugget,
        ]);
        board_state.set(BoardState::ResizeBoardRoot);
    }
}


fn refresh_grid(config: Res<Config>, mut new_elts: Query<(&mut NewElement, &mut Transform)>, r: Res<Rotor>) {
    for (mut new_elt, mut transform) in &mut new_elts {
        new_elt.update_position(&*r);
        transform.translation.x = new_elt.kind.get_real_x(config.board.spirit_size);
        transform.translation.y = new_elt.kind.get_real_y(config.board.spirit_size);
    }
}
