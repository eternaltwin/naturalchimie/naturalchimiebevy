use bevy::prelude::*;
use crate::common::element::{Element, ElementsSheets};

#[derive(Component)]
pub struct BoardEnumUi;

#[derive(Component)]
pub struct ScoreDisplay;

#[derive(Component)]
pub struct GridDisplay;

#[derive(Resource)]
pub struct SpriteSheets {
    pub items: ElementsSheets,
    pub board: BoardSheets,
}

impl SpriteSheets {
    pub fn new(assets: &AssetServer) -> SpriteSheets {
        SpriteSheets {
            items: ElementsSheets::new(assets),
            board: BoardSheets::new(assets),
        }
    }

    pub fn get_all_handles(&self) -> Vec<Handle<Image>> {
        vec![
            self.board.foreground.clone(),
            self.board.background.clone(),
            self.board.bottom_background.clone(),
            self.board.neutral_back.clone(),
            self.board.last_background.clone(),
            self.board.button.clone(),
            self.board.pockets.clone(),
            self.board.next_palette.clone(),
            self.board.items_chain.clone(),
            self.items.handles[0].clone(),
            self.items.handles[1].clone(),
            self.items.handles[2].clone(),
            self.items.handles[3].clone(),
            self.items.handles[4].clone(),
            self.items.handles[5].clone(),
            self.items.handles[6].clone(),
            self.items.handles[7].clone(),
            self.items.handles[8].clone(),
            self.items.handles[9].clone(),
            self.items.handles[10].clone(),
            self.items.handles[11].clone(),
        ]
    }

    pub fn load_board_ui(&self, commands: &mut Commands, elements: [Element; 12]) {
        commands
            .spawn((
                NodeBundle {
                    style: Style {
                        width: Val::Percent(100.),
                        height: Val::Percent(100.),
                        flex_direction: FlexDirection::Row,
                        align_content: AlignContent::Start,
                        justify_content: JustifyContent::Center,

                        ..default()
                    },
                    ..default()
                },
                BoardEnumUi
            ))
            .with_children(|parent| {
                self.display_left_decoration(parent, elements);
                self.display_main_part(parent)
            });
    }

    fn display_left_decoration(&self, parent: &mut ChildBuilder, elements: [Element; 12]) {
        parent
            .spawn((
                NodeBundle {
                    style: Style {
                        width: Val::Px(100.),
                        height: Val::Px(650.),
                        flex_direction: FlexDirection::Column,
                        align_items: AlignItems::Center,
                        justify_content: JustifyContent::Center,
                        ..default()
                    },
                    ..default()
                },
                UiImage::new(self.board.items_chain.clone())
            ))
            .with_children(|parent| {
                elements.iter().for_each(|el| {
                    self.display_item(parent, get_index(el))
                })
            });
    }

    fn display_item(&self, parent: &mut ChildBuilder, el: usize) {
        parent
            .spawn((
                NodeBundle {
                    style: Style {
                        height: Val::Px(40.),
                        width: Val::Px(40.),
                        left: Val::Px(-4.),
                        top: Val::Px(10.),
                        ..default()
                    },
                    ..Default::default()
                },
                UiImage::new(self.items.handles[el].clone())
            ));
    }

    fn display_main_part(&self, parent: &mut ChildBuilder) {
        parent
            .spawn(
                NodeBundle {
                    style: Style {
                        width: Val::Px(477.),
                        flex_direction: FlexDirection::Column,
                        ..default()
                    },
                    ..default()
                }
            )
            .with_children(|parent| {
                parent
                    .spawn((
                        NodeBundle {
                            style: Style {
                                width: Val::Px(477.),
                                height: Val::Px(525.),
                                flex_direction: FlexDirection::Column,
                                ..default()
                            },
                            ..default()
                        },
                        UiImage::new(self.board.background.clone())
                    ))
                    .with_children(|parent| {
                        parent
                            .spawn(NodeBundle {
                                style: Style {
                                    position_type: PositionType::Absolute,
                                    top: Val::Px(102.),
                                    left: Val::Px(93.),
                                    ..default()
                                },
                                ..default()
                            })
                            .with_children(|parent| {
                                self.display_grid(parent);
                                self.display_right_grid_pannel(parent);
                            });
                    });
                self.display_bottom_board(parent);
            });
    }

    fn display_bottom_board(&self, parent: &mut ChildBuilder) {
        parent
            .spawn((
                NodeBundle {
                    style: Style {
                        width: Val::Px(477.),
                        height: Val::Px(104.),
                        ..default()
                    },
                    ..default()
                },
                UiImage::new(self.board.bottom_background.clone())
            ));
    }

    fn display_right_grid_pannel(&self, parent: &mut ChildBuilder) {
        parent
            .spawn(NodeBundle {
                style: Style {
                    flex_direction: FlexDirection::Column,
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                self.display_score(parent);
            });
    }

    fn display_grid(&self, parent: &mut ChildBuilder) {
        parent
            .spawn((
                NodeBundle {
                    style: Style {
                        width: Val::Px(202.),
                        height: Val::Px(356.),
                        margin: UiRect::top(Val::Px(7.)),
                        ..default()
                    },
                    ..default()
                },
                UiImage::new(self.board.foreground.clone()),
                GridDisplay
            ));
    }

    pub fn display_score(&self, parent: &mut ChildBuilder) {
        parent
            .spawn((
                NodeBundle {
                    style: Style {
                        width: Val::Px(104.),
                        height: Val::Px(37.),
                        justify_content: JustifyContent::Center,
                        align_items: AlignItems::Center,
                        ..default()
                    },
                    ..default()
                },
                UiImage::new(self.board.score.clone()),
            ))
            .with_children(|parent|{

            parent.spawn( (
                TextBundle::from_section("0", TextStyle {..default()}),
                ScoreDisplay
            ));
        });
    }
}

fn get_index(el: &Element) -> usize {
    match el {
        Element::GreenPotion => { 0 }
        Element::YellowPotion => { 1 }
        Element::RedPotion => { 2 }
        Element::PurplePotion => { 3 }
        Element::MintLeaf => { 4 }
        Element::MadEye => { 5 }
        Element::Cranos => { 6 }
        Element::Flaum => { 7 }
        Element::Oxide => { 8 }
        Element::Copper => { 9 }
        Element::Mercury => { 10 }
        Element::GoldNugget => { 11 }
    }
}

pub struct BoardSheets {
    pub score: Handle<Image>,
    pub foreground: Handle<Image>,
    pub background: Handle<Image>,
    pub bottom_background: Handle<Image>,
    pub neutral_back: Handle<Image>,
    pub last_background: Handle<Image>,
    pub button: Handle<Image>,
    pub pockets: Handle<Image>,
    pub next_palette: Handle<Image>,
    pub items_chain: Handle<Image>,
}


impl BoardSheets {
    fn new(assets: &AssetServer) -> BoardSheets {
        BoardSheets {
            score: assets.load("images/game/score.png"),
            foreground: assets.load("images/game/gameboard_foreground_original.png"),
            background: assets.load("images/game/client_game_top.png"),
            bottom_background: assets.load("images/game/client_game_bot.png"),
            neutral_back: assets.load("images/game/gameboard_bg.png"),
            last_background: assets.load("images/game/play_bg.png"),
            button: assets.load("images/game/client_button.png"),
            pockets: assets.load("images/game/pockets.png"),
            next_palette: assets.load("images/game/nextPieceBackground.png"),
            items_chain: assets.load("images/game/gameChainRedim.png"),
        }
    }
}