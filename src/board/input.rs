
use bevy::prelude::*;
use crate::board::board::{Board, BoardState};
use crate::board::{create_element, RootBoard};
use crate::board::rotor::{NewElement, Rotor};
use crate::board::score::Score;
use crate::board::sprite::SpriteSheets;
use crate::config::Config;

#[derive(PartialEq, Clone)]
enum InputCommand {
    None,
    Left,
    Right,
    Rotate,
    Fall,
    Pause,
}

#[derive(Resource)]
pub struct InputCommands {
    commands_queue: [InputCommand; 4],
    index: usize,
}


/**
* first step to detach animation and register user commands
*/
impl InputCommands {
    pub fn new() -> Self {
        Self { commands_queue: [InputCommand::None,InputCommand::None,InputCommand::None,InputCommand::None], index: 0 }
    }
    
    fn can_get_another_command(&mut self) -> bool {
        self.commands_queue[(self.index - 1).rem_euclid(4)] == InputCommand::None
    }

    pub fn add(&mut self, command: InputCommand) {
        for x in 0..4 {
            let index = self.index + x;
            if self.commands_queue[index] == InputCommand::None {
                self.commands_queue[index] = command;
                break;
            }
        }
    }

    pub fn consume_command(&mut self) -> InputCommand {
        match self.commands_queue[self.index].clone() {
            InputCommand::None => {
                InputCommand::None
            }
            ic => {
                self.commands_queue[self.index] = InputCommand::None;
                self.index = self.next_index();
                ic
            }
        }
    }

    fn next_index(&mut self) -> usize {
        (self.index + 1).rem_euclid(4)
    }
}


pub fn react_to_user_keyboard_input(
    keys: Res<ButtonInput<KeyCode>>,
    mut input_commands: ResMut<InputCommands>,
    mut config: ResMut<Config>,
    time: Res<Time>,
) {
    if config.timers.input_delay.tick(time.delta()).finished() {
        if keys.any_pressed([KeyCode::KeyP]) {
            input_commands.add(InputCommand::Pause)
        } else if keys.any_pressed([KeyCode::KeyZ, KeyCode::ArrowUp]) {
            input_commands.add(InputCommand::Rotate)
        } else if keys.any_pressed([KeyCode::KeyS, KeyCode::ArrowDown]) {
            input_commands.add(InputCommand::Fall)
        } else if keys.any_pressed([KeyCode::KeyQ, KeyCode::ArrowLeft]) {
            input_commands.add(InputCommand::Left)
        } else if keys.any_pressed([KeyCode::KeyD, KeyCode::ArrowRight]) {
            input_commands.add(InputCommand::Right)
        } else {
            return;
        }
        config.timers.input_delay.reset();
    }
}

fn touches(
    touches: Res<Touches>,
    mut input_commands: ResMut<InputCommands>,
) {
    // There is a lot more information available, see the API docs.
    // This example only shows some very basic things.

    if touches.iter_just_released().count() > 1 {
        input_commands.add(InputCommand::Pause);
    } else {
        for finger in touches.iter_just_released() {
            println!(
                "Finger {} is at position ({},{}), started from ({},{}).",
                finger.id(),
                finger.position().x,
                finger.position().y,
                finger.start_position().x,
                finger.start_position().y,
            );
            let x = finger.position().x - finger.start_position().x;
            let y = finger.position().y - finger.start_position().y;
            if x.abs() > y.abs() {
                if x > 0. {
                    input_commands.add(InputCommand::Right)
                } else {
                    input_commands.add(InputCommand::Left)
                }
            } else {
                if y > 0. {
                    input_commands.add(InputCommand::Rotate)
                } else {
                    input_commands.add(InputCommand::Fall)
                }
            }
        }
    }
}

pub fn apply_input_command(
    mut commands: Commands,
    mut input_commands: ResMut<InputCommands>,
    mut board: ResMut<Board>,
    board_root: Query<Entity, With<RootBoard>>,
    assets: Res<Assets<Image>>,
    sprite_sheet: Res<SpriteSheets>,
    mut new_elts: Query<(Entity, &mut NewElement)>,
    config: ResMut<Config>,
    mut rotor: ResMut<Rotor>,
    mut score: ResMut<Score>,
    board_state: Res<State<BoardState>>,
    mut next_board_state: ResMut<NextState<BoardState>>,
)
{
    match input_commands.consume_command() {
        InputCommand::Right => {
            rotor.move_right(board.grid.len() as i8 - 1);
        }
        InputCommand::Left => {
            rotor.move_left(board.grid.len() as i8 - 1);
        }
        InputCommand::Fall => {
            let entity_board = board_root.get_single().expect("Somewith go wrong at start falling");
            for (entity, new_elt) in &mut new_elts {
                create_element(&mut commands, &mut board, &entity_board, &assets, &sprite_sheet, &config, new_elt.kind.position.clone(), new_elt.kind.kind.clone());
                commands.entity(entity).despawn();
                score.add_element(&new_elt.kind.kind);
            }
            next_board_state.set(BoardState::Gravity);
        }
        InputCommand::Rotate => {
            let mut new_elts_vec = Vec::new();
            for (_, e) in &mut new_elts {
                new_elts_vec.push(e)
            }
            rotor.shift(new_elts_vec);
        }
        InputCommand::Pause => {
            if in_state(BoardState::Pause)(Some(board_state)) {
                next_board_state.set(BoardState::ListenUser)
            } else {
                next_board_state.set(BoardState::Pause)
            }
        }
        InputCommand::None => {}
    };
}