use bevy::prelude::{States};

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
pub enum WorldState {
    #[default]
    Loading,
    Customize,
    Ready
}