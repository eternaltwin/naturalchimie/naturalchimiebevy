pub mod state;

use bevy::color::palettes::css::ANTIQUE_WHITE;
use bevy::prelude::*;
use crate::app_state::AppState;
use crate::common::player::Player;
use crate::common_ui_sprite::{CustomisablePart, WorldSprite, WorldSpriteComponent};
use crate::menu::state::MenuState;
use crate::menu::ui::{cleanup_menu, create_right_menu};
use crate::places::model::Place;
use crate::places::places::Places;
use crate::world::state::{WorldState};

#[derive(Component)]
pub struct PlaceImage;

#[derive(Component)]
pub struct PlaceText;

pub struct WorldPlugin;

impl Plugin for WorldPlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<WorldState>();
        app.add_systems(OnEnter(AppState::World), setup);
        app.add_systems(Update, (loading).run_if(in_state(AppState::World)).run_if(in_state(WorldState::Loading)));
        app.add_systems(Update, (load_menu).run_if(in_state(AppState::World)).run_if(in_state(WorldState::Customize)));
        app.add_systems(OnExit(AppState::World), cleanup_menu);
    }
}

fn setup(mut world_state: ResMut<NextState<WorldState>>) {
    world_state.set(WorldState::Loading);
    println!("loading world");
}

fn load_menu(
    mut commands: Commands,
    mut menu_state: ResMut<NextState<MenuState>>,
    mut world_state: ResMut<NextState<WorldState>>,
    ui_root: Query<Entity, With<CustomisablePart>>,
    sprites: Res<WorldSprite>,
    player: Res<Player>,
    places: Res<Places>
) {
    println!("is empty ? {}", places.places.is_empty());
    if !places.places.is_empty() {
        let entity = ui_root.get_single().expect("Common UI fail to load ");
        commands
            .entity(entity)
            .with_children(|parent| {
                load_place(parent, &sprites, &player, &places);
                create_right_menu(parent, &mut menu_state);
            });
        world_state.set(WorldState::Ready);
    }
}

fn load_place(parent: &mut ChildBuilder, sprites: &WorldSprite, player: &Player, places: &Places) {
    let place = places.get_by_id(&player.location);
    let handle = match sprites.places.places.get(&place.background) {
        Some(handle) => {
            handle.clone()
        }
        None => {
            info!("unsupported location {}", player.location);
            sprites.places.places.get("grenier").unwrap().clone()
        }
    };
    parent.spawn((
        NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                height: Val::Percent(100.),
                width: Val::Px(510.),
                ..default()
            },
            ..default()
        },
    ))
        .with_children(|parent| {
            display_place_img(parent, handle);
            display_text(parent, &place);
        });
}

fn display_place_img(parent: &mut ChildBuilder, handle: Handle<Image>) {
    parent.spawn((
        NodeBundle {
            style: Style {
                height: Val::Px(350.),
                width: Val::Percent(100.),
                ..default()
            },
            ..default()
        },
        UiImage::new(handle),
        PlaceImage
    ));
}

fn display_text(parent: &mut ChildBuilder, place: &Place) {
    parent.spawn((
        NodeBundle {
            style: Style {
                height: Val::Px(350.),
                width: Val::Percent(100.),
                padding: UiRect::all(Val::Px(8.)),
                ..default()
            },
            background_color: BackgroundColor(Srgba::hex("#30201c").unwrap().into()),
            ..default()
        },
        PlaceText
    ))
        .with_children(|parent|{
            parent.spawn(TextBundle::from_section(
                place.desc.to_owned(),
                TextStyle {
                    font_size: 20.0,
                    color: ANTIQUE_WHITE.into(),
                    ..default()
                },
            ));
        });
}

fn loading(mut commands: Commands, mut world_state: ResMut<NextState<WorldState>>, assets: Res<Assets<Image>>, sprites: Res<WorldSprite>) {
    let handles = sprites.get_all_handles();
    let loaded = handles.iter().fold(0, |mut acc, handle| {
        if assets.get(handle).is_some() {
            acc = acc + 1;
        }
        acc
    });
    info!("loading {}/{}", loaded,handles.len());
    if loaded == handles.len() {
        println!("menu loaded");
        sprites.load_world_ui(&mut commands);
        world_state.set(WorldState::Customize);
    }
}
