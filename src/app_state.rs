use bevy::prelude::{Resource, States};
use bevy::utils::HashMap;
use crate::common::element::Element;

#[derive(Resource)]
pub struct Loot{
    pub kubor: u32,
    pub exp: u8,
    pub score: u32,
    pub elements: HashMap<Element, u8>
}

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
pub enum AppState {
    #[default]
    World,
    Quest,
    Map,
    Game,
    ResumeGame,
    RedrawWorld
}
