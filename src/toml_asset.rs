//! Implements loader for a custom asset type.

use bevy::{
    asset::{io::Reader, AssetLoader, LoadContext},
    prelude::*,
    reflect::TypePath,
    utils::BoxedFuture,
};
use bevy::asset::io::AsyncReadExt;
use serde::Deserialize;
use thiserror::Error;
use std;
use std::str::Utf8Error;


#[derive(Debug, Resource)]
pub struct DataAssets {
    pub handle_config: Handle<TomlAsset>,
    pub config_updated: bool
}

#[derive(Asset, TypePath, Debug, Deserialize)]
pub struct TomlAsset(pub String);


#[derive(Default)]
pub struct TomlAssetLoader;

/// Possible errors that can be produced by [`CustomAssetLoader`]
#[non_exhaustive]
#[derive(Debug, Error)]
pub enum TomlAssetLoaderError {
    /// An [IO](std::io) Error
    #[error("Could load Toml: {0}")]
    Io(#[from] std::io::Error),
    #[error("Could parse Toml: {0}")]
    Encode(#[from] Utf8Error),
}

impl AssetLoader for TomlAssetLoader {
    type Asset = TomlAsset;
    type Settings = ();
    type Error = TomlAssetLoaderError;
    fn load<'a>(
        &'a self,
        reader: &'a mut Reader,
        _settings: &'a (),
        _load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<Self::Asset, Self::Error>> {
        Box::pin(async move {
            let mut bytes = Vec::new();
            reader.read_to_end(&mut bytes).await?;
            let data_str = std::str::from_utf8(&bytes)?;
            let asset = TomlAsset( data_str.into() );
            Ok(asset)
        })
    }

    fn extensions(&self) -> &[&str] {
        &["toml"]
    }
}