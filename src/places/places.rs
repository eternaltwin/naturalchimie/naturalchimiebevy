use bevy::prelude::*;
use bevy::utils::HashMap;
use crate::places::model::Place;

#[derive(Resource)]
pub struct Places {
    pub places: HashMap<String, Place>,
    default: Place
}

impl Places {
    pub fn extract_places(contents: &str, acc: &mut Places) {
        for place in Place::from_str(&contents) {
            acc.add_place(place);
        };
    }
    
    pub fn new() -> Places {
        Places { places: Default::default(), default: Place::default() }
    }

    pub fn get_by_id(&self, name: &String) -> &Place {
        self.places.get(name).unwrap_or(&self.default)
    }

    pub fn add_place(&mut self, place: Place) {
        self.places.insert(place.id.clone(), place);
    }
}