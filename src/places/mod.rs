use std::process::exit;
use bevy::color::palettes::css::{MAROON, SILVER, BLUE, BLACK, RED, GREEN};
use bevy::app::{App, Plugin, Startup, Update};
use bevy::asset::{Assets, AssetServer, Handle};
use bevy::ecs::query::QuerySingleError;
use bevy::hierarchy::{BuildChildren, ChildBuilder};
use bevy::input::ButtonInput;
use bevy::log::info;
use bevy::math::{Vec2, Vec3};
use bevy::prelude::*;
use bevy::prelude::Projection::{Orthographic, Perspective};
use bevy::render::camera::ScalingMode;
use bevy::sprite::Anchor;
use bevy::ui::RelativeCursorPosition;
use bevy::utils::tracing::field::display;
use crate::app_state::AppState;
use crate::board::{compute_merge, RootBoard};
use crate::camera_control::{pan_orbit_camera, PanOrbitCamera};
use crate::common::player::Player;
use crate::common_ui_sprite::{CustomisablePart, WorldSprite, WorldSpriteComponent};
use crate::config::{BoardConfig, Config, FileConfig, TimersConfig, View};
use crate::places::places::Places;
use crate::places::state::MapState;
use crate::common::xml_asset::{XmlAsset, XmlAssetLoader};
use crate::toml_asset::{DataAssets, TomlAsset};
use crate::menu::state::MenuState;
use crate::menu::ui::{cleanup_menu, create_right_menu};
use crate::MouseCoords;
use crate::places::map::{LastHovered};
use crate::places::model::{Destination, Place};

pub mod model;
pub mod places;
mod state;
mod map;

#[derive(Component)]
pub struct Map;

pub struct PlacePlugin;

#[derive(Resource)]
struct PlaceXml(pub(crate) Handle<XmlAsset>);

impl Plugin for PlacePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Places::new());
        app
            .add_systems(Startup, setup)
            .add_systems(Update, build_place.run_if(place_is_empty));
        app.init_state::<MapState>();
        app.add_systems(OnEnter(AppState::Map), map::setup);
        app.add_systems(Update, loading.run_if(in_state(AppState::Map)).run_if(in_state(MapState::Loading)));
        app.add_systems(Update, load_interface.run_if(in_state(AppState::Map)).run_if(in_state(MapState::Customize)));
        app.add_systems(Update, (test_refresh, pan_orbit_camera, hover_action).run_if(in_state(AppState::Map)).run_if(in_state(MapState::Ready)));
        app.add_systems(OnExit(AppState::Map), (cleanup_menu, map::clean));
    }
}

fn place_is_empty(places: Res<Places>) -> bool {
    places.places.is_empty()
}

fn load_interface(
    mut commands: Commands,
    mut menu_state: ResMut<NextState<MenuState>>,
    mut world_state: ResMut<NextState<MapState>>,
    ui_root: Query<Entity, With<CustomisablePart>>,
    sprites: Res<WorldSprite>,
    places: Res<Places>,
    player: Res<Player>,
)
{
    let entity = ui_root.get_single().expect("Common UI fail to load ");

    load_place(&mut commands, &sprites, &places, &player);
    commands
        .entity(entity)
        .with_children(|parent| {
            init_map_mask(parent);
            create_right_menu(parent, &mut menu_state);
        });
    world_state.set(MapState::Ready);
}

#[derive(Component)]
struct MapMask;

fn init_map_mask(parent: &mut ChildBuilder) {
    parent.spawn((
        NodeBundle {
            style: Style {
                height: Val::Percent(100.),
                width: Val::Px(510.),
                overflow: Overflow::clip(),
                ..default()
            },
            ..default()
        },
        MapMask
    ))
        .insert(RelativeCursorPosition::default());

}

fn load_place(commands: &mut Commands, sprites: &WorldSprite, places: &Places, player: &Player) {
    let place = places.get_by_id(&player.location);
    let handle = sprites.places.places.get("map").unwrap().clone();
    display_map_backgroud(commands, place, handle);
    for (name, p) in places.places.iter() {
        display_place(commands, &name, &p);
    }
}

fn display_place(commands: &mut Commands, name: &String, place: &Place) {
    info!("place {} at : {},{}", name, place.inf.x, -place.inf.y);
    let entity = SpriteBundle {
        sprite: Sprite {
            color: Srgba::hex("#23282e").unwrap().into(),
            custom_size: Some(Vec2::new(40., 40.)),
            ..Default::default()
        },
        transform: Transform {
            translation: Vec3::new(place.inf.x, -place.inf.y, 1.),
            ..Default::default()
        },
        ..Default::default()
    };
    commands.spawn((
        entity,
        WorldSpriteComponent,
    ));
}

fn test_refresh(mut gizmos: Gizmos, places: Res<Places>, player: Res<Player>) {
    for x in places.places.values() {
        x.dests.iter().for_each(|dest| {
            display_map_path(dest, &mut gizmos, BLUE.into());
        })
    }
    let place = places.get_by_id(&player.location);
    place.dests.iter().for_each(|dest| {
        if dest.to == "stimpk" {
            display_map_path(dest, &mut gizmos, RED.into());
        }
        if dest.to == "guexpr" {
            display_map_path(dest, &mut gizmos, GREEN.into());
        }
        if dest.to == "kirvie" {
            display_map_path(dest, &mut gizmos, BLACK.into());
        }
        if dest.to == "gmprai" {
            display_map_path(dest, &mut gizmos, SILVER.into());
        }
    })
}

fn display_map_path(dest: &Destination, gizmos: &mut Gizmos, color: Color) {
    let vertices = dest.path.iter().map(|value| Vec3 { x: value.x.clone(), y: -value.y.clone(), z: 0.05 }).collect::<Vec<Vec3>>();
    let path = BoxedPolyline3d::new(vertices);
    //println!("path {:?}", path);
    gizmos.primitive_3d(&path, Vec3::new(0., 0., 0.05), Quat::default(), color);
}

fn display_map_backgroud(commands: &mut Commands, place: &Place, handle: Handle<Image>) {
    commands.spawn((
        SpriteBundle {
            transform: Transform {
                scale: Vec3::new(0.42, 0.42, 1.0),
                translation: Vec3::new(-35., -910., 0.),
                ..Default::default()
            },
            texture: handle.clone(),
            sprite: Sprite {
                color: Color::WHITE,
                anchor: Anchor::BottomLeft,
                ..Default::default()
            },
            ..Default::default()
        },
        Map,
        WorldSpriteComponent
    ));
}

fn build_place(
    assets_loader: Res<Assets<XmlAsset>>,
    handle_xml: Res<PlaceXml>,
    mut places: ResMut<Places>
)
{
    let asset = assets_loader.get(&handle_xml.0);
    if asset.is_none() {
        return;
    }
    info!("Custom xml asset loaded");
    Places::extract_places(&asset.unwrap().0, &mut places);

    info!("places loaded")
}

fn cursor_is_on_place(cursor: &MouseCoords, place: &Place) -> bool {
    place.inf.x + 20. >= cursor.0.x &&
        place.inf.x - 20. <= cursor.0.x &&
        (-place.inf.y - 20.) <= cursor.0.y &&
        (-place.inf.y + 20.) >= cursor.0.y
}

#[derive(Component)]
struct HoverPanel;

pub fn hover_action(
    mut commands: Commands,
    places: Res<Places>,
    cursor: ResMut<MouseCoords>,
    mouse: Res<ButtonInput<MouseButton>>,
    entities: Query<Entity, With<HoverPanel>>,
    mask: Query<(Entity, &RelativeCursorPosition), With<MapMask>>,
    mut last_hovered: ResMut<LastHovered>,
    mut player: ResMut<Player>,
    mut state: ResMut<NextState<AppState>>,
    mut q_pan: Query<&mut PanOrbitCamera>,
)
{
    let (root_mask, v) = mask.get_single().expect("error on map mask use");
    let hover_map =  if let Some(relative_cursor_position) = v.normalized {
        relative_cursor_position.x >= 0.
            && relative_cursor_position.x <= 1.
            && relative_cursor_position.y >= 0.
            && relative_cursor_position.y <= 1.
    } else {
        false
    };

    if !hover_map {
        return;
    }
    match &last_hovered.0 {
        Some(id_place) => {
            let place = places.get_by_id(id_place);
            if cursor_is_on_place(&cursor, place) {
                if mouse.any_pressed([MouseButton::Left]) {
                    player.location = place.id.clone();
                    state.set(AppState::World);
                    for e in entities.iter() {
                        commands.entity(e).despawn_recursive();
                    }
                }
            } else {
                last_hovered.0 = None;
                for e in entities.iter() {
                    commands.entity(e).despawn_recursive();
                }
            }
        }
        None => {
            for place in places.places.values() {
                if cursor_is_on_place(&cursor, place) {
                    last_hovered.0 = Some(place.id.clone());
                    commands
                        .entity(root_mask)
                        .with_children(|parent| {
                            let pan = q_pan.get_single().unwrap();
                            let mut x = place.inf.x + 280. - pan.focus.x;
                            let y = place.inf.y + 280. + pan.focus.y;
                            if x > 250. {
                                x -= 130.
                            }
                            parent
                                .spawn((
                                    NodeBundle {
                                        style: Style {
                                            width: Val::Px(80.),
                                            position_type: PositionType::Absolute,
                                            top: Val::Px(y),
                                            left: Val::Px(x),
                                            ..default()
                                        },
                                        background_color: MAROON.into(),
                                        ..default()
                                    },
                                    HoverPanel
                                ))
                                .with_children(|parent| {
                                    parent
                                        .spawn(TextBundle::from_section(place.name.clone(), TextStyle { ..default() }));
                                });
                        });
                }
            }
        }
    }
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    let config_data = asset_server.load("texts/map.xml");
    commands.insert_resource(PlaceXml(config_data));
    info!("place loading")
}

fn loading(mut commands: Commands, mut world_state: ResMut<NextState<MapState>>, assets: Res<Assets<Image>>, sprites: Res<WorldSprite>) {
    let handles = sprites.get_all_handles();
    let loaded = handles.iter().fold(0, |mut acc, handle| {
        if assets.get(handle).is_some() {
            acc = acc + 1;
        }
        acc
    });
    info!("loading {}/{}", loaded,handles.len());
    if loaded == handles.len() {
        println!("menu loaded");
        sprites.load_world_ui(&mut commands);
        world_state.set(MapState::Customize);
    }
}