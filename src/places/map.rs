use bevy::prelude::*;
use crate::camera_control::PanOrbitCamera;
use crate::common::player::Player;
use crate::places::places::Places;
use crate::places::state::MapState;

#[derive(Resource, Default)]
pub struct LastHovered(pub(crate) Option<String>);

pub(crate) fn setup(
    mut commands: Commands,
    mut state: ResMut<NextState<MapState>>,
    mut camera: Query<(&mut Transform, &mut OrthographicProjection, &mut PanOrbitCamera)>,
    player: Res<Player>,
    places: Res<Places>
) {
    commands.init_resource::<LastHovered>();
    let (mut transform, _projection, mut pan) = camera.get_single_mut().unwrap();
    let loc = places.get_by_id(&player.location.to_owned());
    transform.translation.x = loc.inf.x;
    transform.translation.y = -loc.inf.y;
    pan.focus.x = loc.inf.x;
    pan.focus.y = -loc.inf.y;
    state.set(MapState::Loading);
}

pub(crate) fn clean(
    mut commands: Commands,
    mut camera: Query<(&mut Transform, &mut OrthographicProjection), With<PanOrbitCamera>>,
    mut state: ResMut<NextState<MapState>>,
)
{
    let (mut transform, mut projection) = camera.get_single_mut().unwrap();
    transform.translation.x = 0.;
    transform.translation.y = 0.;
    projection.scale = 1.;
    commands.remove_resource::<LastHovered>();
    state.set(MapState::Loading);
}