use std::str::from_utf8;
use bevy::math::Vec2;
use quick_xml::events::attributes::Attribute;
use serde::{Deserialize, Serialize};
use quick_xml::events::Event;
use quick_xml::reader::Reader;
use crate::common::element::Element;
use crate::common::parse_helpers::helpers;

#[derive(Debug, Eq, Hash, PartialEq, Clone, Default)]
pub enum Align {
    #[default]
    Guilde,
    Jeezara,
    ShangKah,
    Audepint,
    Gemini,
    None
}

#[derive(Debug, Eq, PartialEq)]
enum PlaceOption {
    GameOption {},
}

impl Default for PlaceOption {
    fn default() -> Self {
        PlaceOption::GameOption {}
    }
}

#[derive(Debug, PartialEq, Default)]
pub struct Destination {
    pub to: String,
    pub path: Vec<Vec2>,
    pub pa: i32,
}

impl Destination {
    fn new() -> Destination {
        Destination { to: "".to_string(), path: Vec::new(), pa: 0 }
    }
}

#[derive(Debug, PartialEq, Default)]
pub(crate) struct Place {
    pub id: String,
    pub region: i32,
    pub name: String,
    pub align: Align,
    pub inf: Vec2,
    pub background: String,
    pub bg_inf: String,
    pub chain: Vec<Element>,
    pub obj: i32,
    pub chain_known: i32,
    pub artefacts: String,
    pub mod_weight: String,
    pub desc: String,
    pub dests: Vec<Destination>,
}

#[derive(Debug, Eq, PartialEq)]
enum Operation {
    Empty,
    Place,
    Desc,
    Game,
    Move,
}

impl Place {
    pub fn new() -> Place {
        Place {
            id: "".to_string(),
            region: 0,
            name: "".to_string(),
            align: Align::Guilde,
            inf: Vec2::new(0., 0.),
            background: "".to_string(),
            bg_inf: "".to_string(),
            chain: vec![],
            obj: 0,
            chain_known: 0,
            artefacts: "".to_string(),
            mod_weight: "".to_string(),
            desc: "".to_string(),
            dests: vec![],
        }
    }
    pub fn from_str(input: &str) -> Vec<Place> {
        let mut reader = Reader::from_str(input);
        let mut results = Vec::new();
        let mut current = Operation::Empty;
        loop {
            match reader.read_event() {
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                Ok(Event::Eof) => break,
                Ok(Event::Start(value)) => {
                    match value.name().as_ref() {
                        b"place" => {
                            current = Operation::Place;
                            let mut place = Place::new();
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"id" => { place.id = helpers::get_value(&attr) }
                                            b"region" => { place.region = helpers::get_numerical_value(&attr) }
                                            b"name" => { place.name = helpers::get_value(&attr) }
                                            b"align" => { place.align = helpers::get_align(&attr) }
                                            b"inf" => { place.inf = helpers::get_vector_value(&attr) }
                                            b"bg" => { place.background = helpers::get_value(&attr) }
                                            b"bginf" => { place.bg_inf = helpers::get_value(&attr) }
                                            any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?}", e) }
                                }
                            });
                            results.push(place);
                        }
                        b"desc" => { current = Operation::Desc; }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Ok(Event::Empty(value)) => {
                    match value.name().as_ref() {
                        b"game" => {
                            let mut place = results.last_mut().unwrap();
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"chain" => { place.chain = Self::get_chain(&attr) }
                                            b"obj" => { place.obj = helpers::get_numerical_value(&attr) }
                                            b"chainknown" => { place.chain_known = helpers::get_numerical_value(&attr) }
                                            b"artft" => { place.artefacts = helpers::get_value(&attr) }
                                            b"modWeight" => { place.mod_weight = helpers::get_value(&attr) }
                                            any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?}", e) }
                                }
                            });
                        }
                        b"move" => {
                            let mut dest = Destination::new();
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"to" => { dest.to = helpers::get_value(&attr) }
                                            b"road" => { dest.path = helpers::get_vec_vector(&attr) }
                                            b"pa" => { dest.pa = helpers::get_numerical_value(&attr) }
                                            any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?}", e) }
                                }
                            });
                            let mut place = results.last_mut().unwrap();
                            place.dests.push(dest)
                        }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Ok(Event::Text(e)) => {
                    match results.last_mut() {
                        Some(place) => {
                            match current {
                                Operation::Desc => {
                                    place.desc = from_utf8(e.into_inner().as_ref()).unwrap_or("").to_string();
                                    //println!("place.desc {}", place.desc)
                                }
                                _ => {
                                    //for debug purpose
                                    //println!("unparsed {:?} : {:?}", current, from_utf8(e.into_inner().as_ref()).unwrap_or("").to_string())
                                }
                            }
                        }
                        None => {}
                    }
                }
                Ok(Event::End(_)) => {
                    current = Operation::Place;
                }
                _ => {}
            };
        }
        println!("Map parse ended");
        results
    }

    fn get_chain(attr: &Attribute) -> Vec<Element> {
        from_utf8(attr.value.as_ref()).and_then(|item| {
            Ok(item.split(";").map(|item| {
                Element::from_i32(item.parse::<i32>().unwrap())
            }).collect::<Vec<Element>>())
        }).unwrap_or(vec![
            Element::Oxide,
            Element::Copper,
            Element::Mercury,
            Element::GoldNugget,
        ])
    }

}

#[cfg(test)]
mod test {
    use bevy::prelude::Vec2;
    use crate::common::element::Element;
    use crate::places::model::{Align, Destination, Place};

    #[test]
    fn parse_should_return_a_place() {
        let input = r#"<place id="grenie" region="0" name="Grenier aux alchimites" align="gu" inf="856.5:316.9" bg="grenier" bginf="-5:-5">
<game chain="8;9;10;11" obj="0" chainknown="9" artft="Elts(2,null):5000;Dynamit(0):30;Dynamit(1):15;Alchimoth:20" modWeight="Elt(0):2" />
<desc>
Ce vieux grenier isolé est sombre et poussiéreux. Et on dirait qu'il y a plein de bestioles qui grouillent partout !
A se demander qui pourrait bien vouloir habiter ici.
</desc>
<move to="stimpk" pa="0" />
</place>
	"#;
        let places = Place::from_str(input);
        assert_eq!(places.len(), 1);
        assert_eq!(places[0], Place {
            id: "grenie".to_string(),
            region: 0,
            desc: r#"
Ce vieux grenier isolé est sombre et poussiéreux. Et on dirait qu'il y a plein de bestioles qui grouillent partout !
A se demander qui pourrait bien vouloir habiter ici.
"#.to_string(),
            name: "Grenier aux alchimites".to_string(),
            align: Align::Guilde,
            inf: Vec2::new(856.5,316.9),
            background: "grenier".to_string(),
            bg_inf: "-5:-5".to_string(),
            chain: vec!(
                Element::Oxide,
                Element::Copper,
                Element::Mercury,
                Element::GoldNugget
            ),
            obj: 0,
            chain_known: 9,
            artefacts: "Elts(2,null):5000;Dynamit(0):30;Dynamit(1):15;Alchimoth:20".to_string(),
            mod_weight: "Elt(0):2".to_string(),
            dests: vec![
                Destination { to: "stimpk".to_string(), path: Vec::new(), pa: 0 }
            ],
        });
    }
}
