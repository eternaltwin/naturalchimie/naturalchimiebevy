use std::process::exit;
use bevy::prelude::{Resource, TimerMode};
use bevy::time::Timer;
use toml;
use serde::Deserialize;

// Top level struct to hold the TOML texts.
#[derive(Deserialize)]
pub struct FileConfig {
    pub view: View
}

// Config struct holds to texts from the `[config]` section.
#[derive(Deserialize, Clone)]
pub struct View {
    pub x: f32,
    pub y: f32,
    pub frame_per_second: f32
}

pub struct TimersConfig {
    pub input_delay: Timer,
    pub frame_delay: Timer
}

pub struct BoardConfig {
    //grid_size_x: u8,
    //grid_size_y: u8,
    pub spirit_size: f32,
    pub grid_offset_x: f32,
    pub grid_offset_y: f32,
}

#[derive(Resource)]
pub struct Config {
    pub view: View,
    pub timers: TimersConfig,
    pub board: BoardConfig
}

impl Config {
    pub fn extract_toml(contents: &str) -> Config {
        let filename = "./config.toml";

        let data: FileConfig = match toml::from_str(&contents) {
            Ok(d) => d,
            Err(_) => {
                eprintln!("Unable to load texts from `{}`", filename);
                exit(1);
            }
        };

        Config {
            view: data.view.clone(),
            timers: TimersConfig {
                input_delay: Timer::from_seconds(0.25, TimerMode::Once),
                frame_delay: Timer::from_seconds(60.0/ data.view.frame_per_second, TimerMode::Once)
            },
            board: BoardConfig {
                spirit_size: 202./6.,
                grid_offset_x: 420.,
                grid_offset_y: 320.,
            }
        }
    }
}