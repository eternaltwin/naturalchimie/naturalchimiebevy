
use bevy::input::mouse::{MouseMotion, MouseWheel};
use bevy::prelude::*;

/// Tags an entity as capable of panning and orbiting.
#[derive(Component)]
pub struct PanOrbitCamera {
    /// The "focus point" to orbit around. It is automatically updated when panning the camera
    pub focus: Vec2,
    pub radius: f32,
    pub upside_down: bool,
}

impl Default for PanOrbitCamera {
    fn default() -> Self {
        PanOrbitCamera {
            focus: Vec2::ZERO,
            radius: 1.0,
            upside_down: false,
        }
    }
}

const PAN_SPEED: f32 = 700.;

/// Pan the camera with middle mouse click, zoom with scroll wheel, orbit with right mouse click.
pub fn pan_orbit_camera(
    windows: Query<&Window>,
    mut ev_motion: EventReader<MouseMotion>,
    mut ev_scroll: EventReader<MouseWheel>,
    input_mouse: Res<ButtonInput<MouseButton>>,
    mut query: Query<(&mut PanOrbitCamera, &mut Transform, &mut OrthographicProjection)>,
) {
    let pan_button = MouseButton::Left;

    let mut pan = Vec2::ZERO;
    let mut scroll: f32 = 0.0;

    if input_mouse.pressed(pan_button) {
        // Pan only if we're not rotating at the moment
        for ev in ev_motion.read() {
            pan += ev.delta;
        }
    }
    for ev in ev_scroll.read() {
        scroll += ev.y;
    }

    for (mut pan_orbit, mut transform, mut projection) in query.iter_mut() {

        if pan.length_squared() > 0.0 {
            // make panning distance independent of resolution and FOV,
            let window = get_primary_window_size(&windows.get_single().unwrap());
            pan *= Vec2::new(projection.scale * PAN_SPEED, projection.scale* PAN_SPEED) / window;

            pan_orbit.focus.x += -pan.x;
            pan_orbit.focus.y +=  pan.y;
            transform.translation.x = pan_orbit.focus.x;
            transform.translation.y = pan_orbit.focus.y;
        }
        if scroll.abs() > 0.0 {
            pan_orbit.radius -= scroll * pan_orbit.radius * 0.2;
            // dont allow zoom to reach zero or you get stuck
            pan_orbit.radius = f32::max(pan_orbit.radius, 0.05);
            projection.scale = pan_orbit.radius;
        }
    }

    // consume any remaining events, so they don't pile up if we don't need them
    // (and also to avoid Bevy warning us about not checking events every frame update)
    ev_motion.clear();
}

fn get_primary_window_size(window: &Window) -> Vec2 {
    Vec2::new(window.width() as f32, window.height() as f32)
}
