mod state;

use std::ops::Deref;
use bevy::prelude::*;
use bevy::utils::HashMap;
use bevy::utils::tracing::instrument::WithSubscriber;
use crate::app_state::{AppState, Loot};
use crate::common::element::{Element, ElementsSheets};
use crate::common::inventory;
use crate::common::inventory::Inventory;
use crate::common::player::Player;
use crate::common_ui_sprite::{CustomisablePart, WorldSprite, WorldSpriteComponent};
use crate::resume_game::state::{ResumeState};

#[derive(Component)]
struct MenuButton;

#[derive(Component)]
struct ResumeItem;

const BUTTON_TEXT_COLOR: Color = Color::srgb(0.1, 0.1, 0.1);
const TEXT_COLOR: Color = Color::srgb(0.9, 0.9, 0.9);

#[derive(Component)]
enum MenuButtonAction {
    Play,
    Quit,
}

pub struct ResumePlugin;

impl Plugin for ResumePlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<ResumeState>();
        app.add_systems(OnEnter(AppState::ResumeGame), setup);
        app.add_systems(Update, (loading).run_if(in_state(AppState::ResumeGame)).run_if(in_state(ResumeState::Loading)));
        app.add_systems(Update, (load_interface).run_if(in_state(AppState::ResumeGame)).run_if(in_state(ResumeState::Customize)));
        app.add_systems(Update, (menu).run_if(in_state(AppState::ResumeGame)).run_if(in_state(ResumeState::Ready)));
        app.add_systems(OnExit(AppState::ResumeGame), cleanup);
    }
}

fn setup(mut resume_state: ResMut<NextState<ResumeState>>) {
    resume_state.set(ResumeState::Loading);
}

fn load_interface(
    mut commands: Commands,
    mut world_state: ResMut<NextState<ResumeState>>,
    ui_root: Query<Entity, With<CustomisablePart>>,
    sprites: Res<WorldSprite>,
    loot: Res<Loot>,
    mut inventory: ResMut<Inventory>,
    mut player: ResMut<Player>,
)
{
    println!("all loot {:?}", loot.elements);
    if !player.first_play_done && loot.elements.contains_key(&Element::MadEye) {
        player.first_play_done = true;
        player.quest_status.insert("first".to_string(), true);
    } else {
        player.have_create_elements(&loot.elements);
    }
    let entity= ui_root.get_single().expect("Common UI fail to load ");
    inventory.kubor = loot.kubor;
    loot.elements.iter().for_each(|(element, count)| {
       inventory.add_element(element, *count as u32)
    });
    commands
        .entity(entity)
        .with_children(|parent| {
            parent.spawn(NodeBundle {
                style: Style {
                    width: Val::Percent(100.),
                    flex_direction: FlexDirection::Column,
                    margin: UiRect::top(Val::Px(100.)),
                    ..default()
                },
                ..default()
            })
                .with_children(|parent| {
                    display_loot(parent, &sprites, &loot);
                    create_menu(parent,&sprites);
            });
        });
    world_state.set(ResumeState::Ready);
}

fn display_loot(parent: &mut ChildBuilder, common_sprites: &WorldSprite, loot: &Loot) {
    parent.
        spawn((
            NodeBundle {
                style: Style {
                    left: Val::Px(100.),
                    width: Val::Px(500.),
                    display: Display::Grid,
                    grid_template_columns: vec![GridTrack::px(100.), GridTrack::px(30.), GridTrack::px(300.),],
                    grid_template_rows: vec![GridTrack::px(50.), GridTrack::px(50.), GridTrack::px(50.), GridTrack::auto()],
                    ..default()
                },
                ..default()
            },
            ResumeItem
        ))
        .with_children(|parent| {
            display_score(parent, "Score".to_string(),loot.score.to_string());
            display_row(parent, "Experience".to_string(),loot.exp.to_string());
            display_row(parent, "Kubors".to_string(),loot.kubor.to_string());
            display_elements(parent, &loot.elements, &common_sprites.items);

        });
    // display_row(&mut parent, TextBundle::from_section("Reputations :", style.clone()), TextBundle::from_section(loot.exp.to_string(), style.clone()));
}

fn display_score(parent: &mut ChildBuilder, description: String, value: String) {
    // for now just a text like other field. Looking about the between :
    // - solution to emul css property
    let style = TextStyle {
        font_size: 20.0,
        color: TEXT_COLOR,

        ..default()
    };
    parent
        .spawn((
            TextBundle::from_section(description, style.clone()).with_text_justify(JustifyText::Right),
            ResumeItem
        ));
    parent
        .spawn((
            TextBundle::from_section(":", style.clone()).with_text_justify(JustifyText::Center),
            ResumeItem
        ));
    parent
        .spawn((
            TextBundle::from_section(value, style),
            ResumeItem
        ));
}

fn display_elements(parent: &mut ChildBuilder, value: &HashMap<Element, u8>, asset: &ElementsSheets) {
    // for now just a text like other field. Looking about the between :
    // - solution to emul css property
    let style = TextStyle {
        font_size: 20.0,
        color: TEXT_COLOR,
        ..default()
    };
    parent
        .spawn((
            TextBundle::from_section("Elements", style.clone()).with_text_justify(JustifyText::Right),
            ResumeItem
        ));
    parent
        .spawn((
            TextBundle::from_section(":", style.clone()).with_text_justify(JustifyText::Center),
            ResumeItem
        ));
    parent
        .spawn((
            NodeBundle {
                ..default()
            },
            ResumeItem
        ))
        .with_children(|parent| {
            let mut ordered_counted_elements = value.iter().map(|(a,c)| (a.clone(), c.clone())).collect::<Vec<(Element, u8)>>();
            ordered_counted_elements.sort_by(|(a,_),(b,_)|  b.as_index().cmp(&a.as_index()));

            ordered_counted_elements
                .into_iter()
                .for_each(|(element, count)|{
                    parent
                        .spawn((
                            NodeBundle {
                                style: Style {
                                    width: Val::Px(50.),
                                    height: Val::Px(50.),
                                  ..default()
                                },
                                ..default()
                            },
                            ResumeItem
                        ))
                        .with_children(|node| {
                            node
                                .spawn((
                                    ImageBundle {
                                        image: UiImage::from(element.get_texture(asset).clone()),
                                        ..default()
                                    },
                                    ResumeItem
                                ));
                            let text = TextBundle::from_section(count.to_string(), style.clone())
                                .with_style(
                                    Style{
                                        position_type: PositionType::Absolute,
                                        left: Val::Px(0.),
                                        bottom: Val::Px(0.),
                                        ..default()
                                    }
                                );
                            node
                                .spawn((
                                    text,
                                    ResumeItem
                                ));
                        });
                })
        });
}

fn display_row(parent: &mut ChildBuilder, description: String, value: String) {
    let style = TextStyle {
        font_size: 20.0,
        color: TEXT_COLOR,

        ..default()
    };
    parent
        .spawn((
            TextBundle::from_section(description, style.clone()).with_text_justify(JustifyText::Right),
            ResumeItem
        ));
    parent
        .spawn((
            TextBundle::from_section(":", style.clone()).with_text_justify(JustifyText::Center),
            ResumeItem
        ));
    parent
        .spawn((
            TextBundle::from_section(value, style),
            ResumeItem
        ));
}

fn create_menu(parent: &mut ChildBuilder, common_sprites: &WorldSprite) {
    let button_style = Style {
        width: Val::Px(200.0),
        height: Val::Px(30.0),
        margin: UiRect::all(Val::Px(20.0)),
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        ..default()
    };
    parent
        .spawn((NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Row,
                ..default()
            },
            ..default()
        },
                ResumeItem
        ))
        .with_children(|parent| {
            parent
                .spawn((
                    ButtonBundle {
                        style: button_style.clone(),
                        image: UiImage::from(common_sprites.buttons.button_next.clone()).with_flip_x(),
                        ..default()
                    },
                    MenuButtonAction::Play,
                    MenuButton
                ))
                .with_children(|parent| {
                    parent.spawn((
                        TextBundle::from_section(
                            "Rejouer",
                            TextStyle {
                                font_size: 20.0,
                                color: BUTTON_TEXT_COLOR,
                                ..default()
                            },
                        ), MenuButton
                    ));
                });

            parent
                .spawn((
                    ButtonBundle {
                        style: button_style.clone(),
                        image: UiImage::from(common_sprites.buttons.button_next.clone()),
                        ..default()
                    },
                    MenuButtonAction::Quit,
                    MenuButton
                ))
                .with_children(|parent| {
                    parent.spawn((
                        TextBundle::from_section(
                            "Continuer",
                            TextStyle {
                                font_size: 20.0,
                                color: BUTTON_TEXT_COLOR,
                                ..default()
                            },
                        ), MenuButton
                    ));
                });
        });
}

fn menu(
    interaction_query: Query<
        (&Interaction, &MenuButtonAction),
        (Changed<Interaction>, With<Button>),
    >,
    mut state: ResMut<NextState<AppState>>,
)
{
    for (interaction, menu_button_action) in &interaction_query {
        if *interaction == Interaction::Pressed {
            match menu_button_action {
                MenuButtonAction::Play => {
                    state.set(AppState::Game);
                }
                MenuButtonAction::Quit => {
                    state.set(AppState::World);
                }
            }
        }
    }
}

fn cleanup(mut commands: Commands, sprite: Query<Entity, With<WorldSpriteComponent>>) {
    println!("exit resume menu");
    for e in sprite.iter() {
        commands.entity(e).despawn_recursive();
    }
}

fn loading(mut commands: Commands, mut world_state: ResMut<NextState<ResumeState>>, assets: Res<Assets<Image>>, sprites: Res<WorldSprite>) {
    let handles = sprites.get_all_handles();
    let loaded = handles.iter().fold(0, |mut acc, handle| {
        if assets.get(handle).is_some() {
            acc = acc + 1;
        }
        acc
    });
    info!("loading {}/{}", loaded,handles.len());
    if loaded == handles.len() {
        println!("menu loaded");
        sprites.load_world_ui(&mut commands);
        world_state.set(ResumeState::Customize);
    }
}
