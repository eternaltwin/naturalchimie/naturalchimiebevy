use bevy::prelude::{States};

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
pub enum ResumeState {
    #[default]
    Loading,
    Customize,
    Ready
}