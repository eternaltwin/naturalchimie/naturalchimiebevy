use bevy::app::{App, Plugin};
use bevy::prelude::*;
use crate::menu::action::menu;
use crate::menu::state::MenuState;
use crate::menu::system::draw_menu;

pub mod state;
pub mod ui;
mod action;
mod system;

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<MenuState>()
            .add_systems(OnEnter(MenuState::Drawing), draw_menu)
            .add_systems(Update, menu.run_if(in_state(MenuState::Interactive)));
        //not necessary with recursive remove ?
        //app.add_systems(OnExit(AppState::World), cleanup_menu);
    }
}
