use bevy::prelude::*;
use bevy::color::palettes::css::ANTIQUE_WHITE;
use bevy::utils::HashMap;
use crate::common::inventory::Inventory;
use crate::common::player::Player;
use crate::common_ui_sprite::{WorldSprite, WorldSpriteComponent};
use crate::menu::action::MenuButtonAction;
use crate::menu::state::MenuState;
use crate::quests::dialogs::Dialogs;
use crate::quests::quests::Quests;
use crate::quests::ui_dialogs::CurrentDialog;
use crate::quests::state::PnjInteractionState;

#[derive(Component)]
pub struct MenuButton;

#[derive(Component)]
pub struct MenuButtonContainer;

const NORMAL_BUTTON: Color = Color::srgb(0.15, 0.15, 0.15);
const TEXT_COLOR: Color = Color::srgb(0.7, 0.7, 0.7);


pub fn draw_menu_content(
    parent: &mut ChildBuilder,
    sprite: &WorldSprite,
    player: &Player,
    dialogs: &Dialogs,
    quests: &Quests,
    inventory: &Inventory,
    current_dialog: &mut CurrentDialog,
    quests_state: &mut NextState<PnjInteractionState>,
) {
    let button_style = Style {
        width: Val::Px(186.),
        height: Val::Px(50.),
        margin: UiRect::all(Val::Px(20.0)),
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        ..default()
    };
        create_menu_button(sprite, button_style.clone(), parent, sprite.buttons.icon_play.clone(), "Jouer", MenuButtonAction::Play);
        create_menu_button(sprite, button_style.clone(), parent, sprite.buttons.icon_move.clone(), "Se déplacer", MenuButtonAction::Map);
        let dialogs = dialogs.get_dialogs_for(player, &quests.meta, inventory);
        let auto = dialogs.iter().find(|d| d.auto);
    println!("==========");
    println!("Dialogs: {:?}", dialogs.iter().map(|d|d.name.to_string()).collect::<Vec<String>>());
    println!("one auto ?: {:?}", auto.is_some());
        if auto.is_some() {
            quests_state.set(PnjInteractionState::Dialog);
            current_dialog.id = auto.unwrap().name.to_string();
            current_dialog.pnj_key = "begin".to_string();
        } else {
            for dialog in dialogs {
                println!("create menu button for {}, {} ", dialog.name, dialog.from);
                let name = if dialog.from.is_empty() { &dialog.name } else { &dialog.from };
                create_menu_button(sprite, button_style.clone(), parent, sprite.buttons.icon_talk.clone(), name, MenuButtonAction::Talk { dialog: name.clone() });
            };
        }
}

pub fn create_right_menu(parent: &mut ChildBuilder, state: &mut NextState<MenuState>) {
    parent
        .spawn((NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                align_items: AlignItems::Center,
                width: Val::Px(195.),
                height: Val::Percent(100.),
                ..default()
            },
            background_color: Srgba::hex("#a38c73").unwrap().into(),
            ..default()
        }, MenuButtonContainer));
    state.set(MenuState::Drawing);
}

fn create_menu_button(sprite: &WorldSprite, button_style: Style, parent: &mut ChildBuilder, handle: Handle<Image>, text: &str, action: MenuButtonAction) {
    parent
        .spawn((
            ButtonBundle {
                style: button_style.clone(),
                image: UiImage::new(sprite.buttons.play_menu.clone()),
                ..default()
            },
            action,
            MenuButton
        ))
        .with_children(|parent| {
            parent.spawn((
                NodeBundle {
                    style: Style {
                        position_type: PositionType::Absolute,
                        left: Val::Px(10.),
                        top: Val::Px(5.),
                        height: Val::Px(40.),
                        width: Val::Px(40.),
                        ..default()
                    },
                    ..default()
                },
                UiImage::new(handle)
            ));
            parent.spawn((
                TextBundle::from_section(
                    text,
                    TextStyle {
                        font_size: 20.0,
                        color: ANTIQUE_WHITE.into(),
                        ..default()
                    },
                ), MenuButton
            ));
        });
}


pub fn cleanup_menu(mut commands: Commands, entities: Query<Entity, With<MenuButton>>, sprite: Query<Entity, With<WorldSpriteComponent>>) {
    println!("exit Menu. remove {} entities", entities.iter().count());
    for e in entities.iter() {
        commands.entity(e).despawn_recursive();
    }
    for e in sprite.iter() {
        commands.entity(e).despawn_recursive();
    }
}