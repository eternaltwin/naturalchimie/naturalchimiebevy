use bevy::log::info;
use bevy::prelude::{Button, Changed, Component, Interaction, NextState, Query, ResMut, With};
use crate::app_state::AppState;
use crate::menu::state::MenuState;
use crate::quests::ui_dialogs::CurrentDialog;
use crate::quests::state::PnjInteractionState;

#[derive(Component, Debug)]
pub enum MenuButtonAction {
    Play,
    Map,
    Talk{dialog: String},
    Chouettex,
    Ranking,
    Guildian,
    Bank,
    Help,
}

pub fn menu(
    interaction_query: Query<
        (&Interaction, &MenuButtonAction),
        (Changed<Interaction>, With<Button>),
    >,
    mut current_dialog: ResMut<CurrentDialog>,
    mut state: ResMut<NextState<AppState>>,
    mut quests_state: ResMut<NextState<PnjInteractionState>>,
    mut menu_state: ResMut<NextState<MenuState>>,
) {
    // see https://github.com/bevyengine/bevy/blob/main/examples/games/game_menu.rs
    for (interaction, menu_button_action) in &interaction_query {
        if *interaction == Interaction::Pressed {
            match menu_button_action {
                MenuButtonAction::Play => {
                    println!("Play");
                    state.set(AppState::Game); // add here the custom element ?
                    menu_state.set(MenuState::None);
                }
                MenuButtonAction::Map => {
                    println!("Map");
                    state.set(AppState::Map);
                    menu_state.set(MenuState::None);
                }
                MenuButtonAction::Talk{dialog} => {
                    println!("Talk");
                    state.set(AppState::World);
                    quests_state.set(PnjInteractionState::Dialog);
                    current_dialog.id = dialog.to_string();
                    current_dialog.pnj_key = "begin".to_string();
                    menu_state.set(MenuState::Interactive);
                }
                _ => { info!("Menu interaction not implemented") }
            }
        }
    }
}