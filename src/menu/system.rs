use bevy::prelude::*;
use crate::common::inventory::Inventory;
use crate::common::player::Player;
use crate::common_ui_sprite::{WorldSprite};
use crate::menu::state::MenuState;
use crate::menu::ui::{draw_menu_content, MenuButtonContainer};
use crate::quests::dialogs::{Dialogs};
use crate::quests::quests::Quests;
use crate::quests::ui_dialogs::CurrentDialog;
use crate::quests::state::PnjInteractionState;

pub fn draw_menu(
    mut commands: Commands,
    mut state: ResMut<NextState<MenuState>>,
    ui_root: Query<Entity, With<MenuButtonContainer>>,
    player: Res<Player>,
    dialogs: Res<Dialogs>,
    quests: Res<Quests>,
    sprites: Res<WorldSprite>,
    inventory: Res<Inventory>,
    mut current_dialog: ResMut<CurrentDialog>,
    mut quests_state: ResMut<NextState<PnjInteractionState>>,
) {
    let entity_root = ui_root.get_single().expect("Common UI fail to load ");
    commands
        .entity(entity_root)
        .with_children(|parent| {
            draw_menu_content(parent, &sprites, &player, &dialogs, &quests, &inventory, &mut current_dialog, &mut quests_state)
        });
    state.set(MenuState::Interactive);
}