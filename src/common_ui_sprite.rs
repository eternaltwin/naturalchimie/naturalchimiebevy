use bevy::prelude::*;
use bevy::sprite::Anchor;
use bevy::utils::HashMap;
use crate::common::element::ElementsSheets;
use crate::common::player::Player;
use crate::quests::quests::{Quest, Quests};

#[derive(Component)]
pub struct WorldSpriteComponent;

#[derive(Component)]
pub struct CustomisablePart;

#[derive(Component)]
pub struct AvatarColumnUI;


#[derive(Resource)]
pub struct WorldSprite {
    pub items: ElementsSheets,
    pub ui: UiSheets,
    pub menu: MenuSheets,
    pub places: PlacesSheets,
    pub buttons: ButtonsSprites,
}

impl WorldSprite {
    pub fn new(assets: &AssetServer) -> WorldSprite {
        WorldSprite {
            items: ElementsSheets::new(assets),
            ui: UiSheets::new(assets),
            menu: MenuSheets::new(assets),
            places: PlacesSheets::new(assets),
            buttons: ButtonsSprites::new(assets),
        }
    }

    pub fn get_all_handles(&self) -> Vec<Handle<Image>> {
        vec![
            self.ui.background.clone(),
            self.ui.body_left.clone(),
            self.ui.header.clone(),
            self.menu.generic.clone(),
            self.menu.available_custom_items.clone(),
            self.menu.bank.clone(),
            self.menu.guildian.clone(),
            self.menu.chouettex.clone(),
            self.menu.map.clone(),
            self.menu.help.clone(),
            self.menu.header.clone(),
        ]
    }


    pub fn load_world_ui(&self, commands: &mut Commands) {
        commands
            .spawn((
                NodeBundle {
                    style: Style {
                        width: Val::Percent(100.),
                        height: Val::Percent(100.),
                        ..default()
                    },
                    ..default()
                },
                WorldSpriteComponent
            ))
            .with_children(|parent| {
                self.spawn_left_decorations(parent);
                self.spawn_body(parent);
                self.spawn_right_decorations(parent);
            });
    }

    fn spawn_left_decorations(&self, parent: &mut ChildBuilder) {
        parent
            .spawn(NodeBundle {
                style: Style {
                    flex_grow: 1.,
                    ..default()
                },
                background_color: Srgba::hex("#181815").unwrap().into(),
                ..default()
            });
    }

    fn spawn_body(&self, parent: &mut ChildBuilder) {
        parent.spawn(NodeBundle {
            style: Style {
                width: Val::Px(905.),
                height: Val::Percent(100.),
                flex_direction: FlexDirection::Column,
                ..default()
            },
            ..default()
        })
            .with_children(|parent| {
                self.display_header(parent);
                parent.spawn(NodeBundle {
                    style: Style {
                        width: Val::Percent(100.),
                        height: Val::Percent(100.),
                        ..default()
                    },
                    ..default()
                })
                    .with_children(|parent| {
                        self.build_avatar_column(parent);
                        println!("CustomisablePart up");
                        parent
                            .spawn((
                                NodeBundle {
                                    style: Style {
                                        flex_grow: 1.,
                                        ..default()
                                    },
                                    ..default()
                                },
                                CustomisablePart
                            ));
                    });
            });
    }

    fn build_avatar_column(&self, parent: &mut ChildBuilder) {
        parent.spawn((
            NodeBundle {
                style: Style {
                    width: Val::Px(207.),
                    height: Val::Percent(100.),
                    ..default()
                },
                background_color: Srgba::hex("#181815").unwrap().into(),
                ..default()
            },
        ))
            .with_children(|parent| {
                parent
                    .spawn((
                        NodeBundle {
                            style: Style {
                                width: Val::Px(207.),
                                height: Val::Px(592.),
                                ..default()
                            },
                            ..default()
                        },
                        UiImage::new(self.ui.avatar_foreground.clone()),
                        AvatarColumnUI
                    ));
            });
    }

    fn display_header(&self, parent: &mut ChildBuilder) {
        parent
            .spawn(NodeBundle {
                style: Style {
                    position_type: PositionType::Relative,
                    height: Val::Px(103.),
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                parent
                    .spawn((
                        NodeBundle {
                            style: Style {
                                position_type: PositionType::Absolute,
                                width: Val::Px(844.),
                                height: Val::Px(103.),
                                left: Val::Px(0.),
                                top: Val::Px(0.),
                                ..default()
                            },
                            ..default()
                        },
                        UiImage::new(self.ui.header.clone())
                    ));
                parent
                    .spawn((
                        NodeBundle {
                            style: Style {
                                width: Val::Px(415.),
                                height: Val::Px(81.),
                                position_type: PositionType::Absolute,
                                right: Val::Px(0.),
                                bottom: Val::Px(0.),
                                ..default()
                            },
                            ..default()
                        },
                        UiImage::new(self.menu.header.clone())
                    ));
            });
    }

    fn spawn_right_decorations(&self, parent: &mut ChildBuilder) {
        parent
            .spawn(NodeBundle {
                style: Style {
                    flex_grow: 1.,
                    ..default()
                },
                background_color: Srgba::hex("#181815").unwrap().into(),
                ..default()
            });
    }


    pub fn load_ui_part_at(commands: &mut Commands, texture: Handle<Image>, translation: Vec3) {
        let bundle = SpriteBundle {
            transform: Transform {
                translation,
                ..Default::default()
            },
            texture,
            sprite: Sprite {
                anchor: Anchor::BottomLeft,
                ..Default::default()
            },
            ..Default::default()
        };
        commands.spawn((bundle, WorldSpriteComponent {}));
    }
}

pub struct UiSheets {
    pub background: Handle<Image>,
    pub body_left: Handle<Image>,
    //pub body_right: Handle<Image>, not in asset for now ?
    pub header: Handle<Image>,
    pub avatar_foreground: Handle<Image>,
    pub currency_card_background: Handle<Image>,
    pub kubor: Handle<Image>,
    pub lv_icon: Handle<Image>,
    pub pa_flask: Handle<Image>,
    pub pyram: Handle<Image>,
    pub quest_bg_transparent: Handle<Image>,
    pub sub_menu_bg_transparent: Handle<Image>,
    pub sub_menu_fo: Handle<Image>,
    pub sub_menu_grim: Handle<Image>,
    pub sub_menu_inventory: Handle<Image>,
    pub sub_menu_school: Handle<Image>,
    pub xp_flask_merged: Handle<Image>,
    pub xp_flask: Handle<Image>,
}

impl UiSheets {
    fn new(assets: &AssetServer) -> UiSheets {
        UiSheets {
            background: assets.load("images/ui/bg.png"),
            body_left: assets.load("images/ui/body_left.png"),
            //body_right: assets.load("images/ui/mainMenu_bank.png"),
            header: assets.load("images/ui/headerLogo.png"),
            avatar_foreground: assets.load("images/ui/leftMenu/AvatarForeground.png"),
            currency_card_background: assets.load("images/ui/leftMenu/CurrencyCardBackground.png"),
            kubor: assets.load("images/ui/leftMenu/kubor.png"),
            lv_icon: assets.load("images/ui/leftMenu/LvIcon.png"),
            pa_flask: assets.load("images/ui/leftMenu/PaFlask.png"),
            pyram: assets.load("images/ui/leftMenu/pyram.png"),
            quest_bg_transparent: assets.load("images/ui/leftMenu/quest_bg_transparent.png"),
            sub_menu_bg_transparent: assets.load("images/ui/leftMenu/sub_menu_bg_transparent.png"),
            sub_menu_fo: assets.load("images/ui/leftMenu/sub_menu_fo.png"),
            sub_menu_grim: assets.load("images/ui/leftMenu/sub_menu_grim.png"),
            sub_menu_inventory: assets.load("images/ui/leftMenu/sub_menu_inventory.png"),
            sub_menu_school: assets.load("images/ui/leftMenu/sub_menu_school.png"),
            xp_flask_merged: assets.load("images/ui/leftMenu/XpFlask-Merged.png"),
            xp_flask: assets.load("images/ui/leftMenu/XpFlask.png"),
        }
    }
}


pub struct MenuSheets {
    pub generic: Handle<Image>,
    pub available_custom_items: Handle<Image>,
    pub bank: Handle<Image>,
    pub guildian: Handle<Image>,
    pub chouettex: Handle<Image>,
    pub map: Handle<Image>,
    pub help: Handle<Image>,
    //pub background: Handle<Image>, not in assets for now?
    pub header: Handle<Image>,
}

impl MenuSheets {
    fn new(assets: &AssetServer) -> MenuSheets {
        MenuSheets {
            generic: assets.load("images/ui/mainMenu_bank.png"),
            available_custom_items: assets.load("images/ui/cartouche_elem.png"),
            bank: assets.load("images/ui/mainMenu_bank.png"),
            guildian: assets.load("images/ui/mainMenu_guildian.png"),
            chouettex: assets.load("images/ui/mainMenu_cex.png"),
            map: assets.load("images/ui/mainMenu_map.png"),
            help: assets.load("images/ui/mainMenu_help.png"),
            //background: assets.load("images/ui/pockets.png"),
            header: assets.load("images/ui/header_v2.png"),
        }
    }
}

pub struct PlacesSheets {
    pub places: HashMap<String, Handle<Image>>,
}

impl PlacesSheets {
    fn new(assets: &AssetServer) -> PlacesSheets {
        let mut places = HashMap::new();
        places.insert("agora".to_string(), assets.load("images/locations/agora.png"));
        places.insert("aparmy".to_string(), assets.load("images/locations/aparmy.png"));
        places.insert("apchl".to_string(), assets.load("images/locations/apchl.png"));
        places.insert("apcol".to_string(), assets.load("images/locations/apcol.png"));
        places.insert("apfbg".to_string(), assets.load("images/locations/apfbg.png"));
        places.insert("aphall".to_string(), assets.load("images/locations/aphall.png"));
        places.insert("aphome".to_string(), assets.load("images/locations/aphome.png"));
        places.insert("aplib".to_string(), assets.load("images/locations/aplib.png"));
        places.insert("apterr".to_string(), assets.load("images/locations/apterr.png"));
        places.insert("apvos".to_string(), assets.load("images/locations/apvos.png"));
        places.insert("bafa".to_string(), assets.load("images/locations/bafa.png"));
        places.insert("cauldron".to_string(), assets.load("images/locations/cauldron.png"));
        places.insert("chouettex".to_string(), assets.load("images/locations/chouettex.png"));
        places.insert("enfa".to_string(), assets.load("images/locations/enfa.png"));
        places.insert("gmclim".to_string(), assets.load("images/locations/gmclim.png"));
        places.insert("gmfal".to_string(), assets.load("images/locations/gmfal.png"));
        places.insert("gmhome".to_string(), assets.load("images/locations/gmhome.png"));
        places.insert("gmkork".to_string(), assets.load("images/locations/gmkork.png"));
        places.insert("gmkrom".to_string(), assets.load("images/locations/gmkrom.png"));
        places.insert("gmoree".to_string(), assets.load("images/locations/gmoree.png"));
        places.insert("gmprai".to_string(), assets.load("images/locations/gmprai.png"));
        places.insert("gmsch".to_string(), assets.load("images/locations/gmsch.png"));
        places.insert("grenier".to_string(), assets.load("images/locations/grenier.png"));
        places.insert("guildian".to_string(), assets.load("images/locations/guildian.png"));
        places.insert("guwell".to_string(), assets.load("images/locations/guwell.png"));
        places.insert("jzboy".to_string(), assets.load("images/locations/jzboy.png"));
        places.insert("jzcata".to_string(), assets.load("images/locations/jzcata.png"));
        places.insert("jzclai".to_string(), assets.load("images/locations/jzclai.png"));
        places.insert("jzdoor".to_string(), assets.load("images/locations/jzdoor.png"));
        places.insert("jzhome".to_string(), assets.load("images/locations/jzhome.png"));
        places.insert("jzlabo".to_string(), assets.load("images/locations/jzlabo.png"));
        places.insert("jzsaka".to_string(), assets.load("images/locations/jzsaka.png"));
        places.insert("jztire".to_string(), assets.load("images/locations/jztire.png"));
        places.insert("jzvent".to_string(), assets.load("images/locations/jzvent.png"));
        places.insert("jzzig".to_string(), assets.load("images/locations/jzzig.png"));
        places.insert("kirvie".to_string(), assets.load("images/locations/kirvie.png"));
        places.insert("map".to_string(), assets.load("images/locations/map.png"));
        places.insert("scroad".to_string(), assets.load("images/locations/scroad.png"));
        places.insert("skflak".to_string(), assets.load("images/locations/skflak.png"));
        places.insert("skguic".to_string(), assets.load("images/locations/skguic.png"));
        places.insert("skhoma".to_string(), assets.load("images/locations/skhoma.png"));
        places.insert("skhomb".to_string(), assets.load("images/locations/skhomb.png"));
        places.insert("skmedi".to_string(), assets.load("images/locations/skmedi.png"));
        places.insert("skoasi".to_string(), assets.load("images/locations/skoasi.png"));
        places.insert("skobs".to_string(), assets.load("images/locations/skobs.png"));
        places.insert("skshar".to_string(), assets.load("images/locations/skshar.png"));
        places.insert("sktupu".to_string(), assets.load("images/locations/sktupu.png"));
        places.insert("steampunk".to_string(), assets.load("images/locations/steampunk.png"));
        PlacesSheets { places }
    }
}

pub struct ButtonsSprites {
    pub act_menu_blocked: Handle<Image>,
    pub act_menu_hover: Handle<Image>,
    pub act_menu: Handle<Image>,
    pub button_back2map_hover: Handle<Image>,
    pub button_back2map: Handle<Image>,
    pub button_next_hover: Handle<Image>,
    pub button_next: Handle<Image>,
    pub guildian: Handle<Image>,
    pub icon_down: Handle<Image>,
    pub icon_move: Handle<Image>,
    pub icon_play: Handle<Image>,
    pub icon_rank: Handle<Image>,
    pub icon_recipe: Handle<Image>,
    pub icon_shop: Handle<Image>,
    pub icon_sleep: Handle<Image>,
    pub icon_talk: Handle<Image>,
    pub icon_up: Handle<Image>,
    pub play_menu: Handle<Image>,
    pub play_menu_hover: Handle<Image>,
    pub snb_next_hover: Handle<Image>,
    pub snb_next: Handle<Image>,
    pub snb_prev_hover: Handle<Image>,
    pub snb_prev: Handle<Image>,
    pub w_button: Handle<Image>,
}

impl ButtonsSprites {
    fn new(assets: &AssetServer) -> ButtonsSprites {
        ButtonsSprites {
            act_menu_blocked: assets.load("images/buttons/act_menu_blocked.png"),
            act_menu_hover: assets.load("images/buttons/act_menu_hover.png"),
            act_menu: assets.load("images/buttons/act_menu.png"),
            button_back2map_hover: assets.load("images/buttons/button_back2map_hover.png"),
            button_back2map: assets.load("images/buttons/button_back2map.png"),
            button_next_hover: assets.load("images/buttons/button_next_hover.png"),
            button_next: assets.load("images/buttons/button_next.png"),
            guildian: assets.load("images/buttons/guildian.png"),
            icon_down: assets.load("images/buttons/icon_down.png"),
            icon_move: assets.load("images/buttons/icon_move.png"),
            icon_play: assets.load("images/buttons/icon_play.png"),
            icon_rank: assets.load("images/buttons/icon_rank.png"),
            icon_recipe: assets.load("images/buttons/icon_recipe.png"),
            icon_shop: assets.load("images/buttons/icon_shop.png"),
            icon_sleep: assets.load("images/buttons/icon_sleep.png"),
            icon_talk: assets.load("images/buttons/icon_talk.png"),
            icon_up: assets.load("images/buttons/icon_up.png"),
            play_menu: assets.load("images/buttons/play_menu.png"),
            play_menu_hover: assets.load("images/buttons/play_menu_hover.png"),
            snb_next_hover: assets.load("images/buttons/snb_next_hover.png"),
            snb_next: assets.load("images/buttons/snb_next.png"),
            snb_prev_hover: assets.load("images/buttons/snb_prev_hover.png"),
            snb_prev: assets.load("images/buttons/snb_prev.png"),
            w_button: assets.load("images/buttons/wButton.png"),
        }
    }
}