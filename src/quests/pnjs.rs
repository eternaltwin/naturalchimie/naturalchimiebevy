use std::str::from_utf8;
use bevy::prelude::Resource;
use bevy::utils::HashMap;
use quick_xml::events::Event;
use quick_xml::reader::Reader;
use crate::common::parse_helpers::helpers;
use crate::places::model::Align;

#[derive(Debug, Eq, PartialEq, Default)]
pub struct QuestPNJ {
    pub name: String,
    pub asset: String,
    pub align: Align,
    pub place: String,
    pub id: String,
    pub gid: String,
}

#[derive(Resource)]
pub struct PjnQuests {
    pnjs: HashMap<String, QuestPNJ>,
}

impl QuestPNJ {
    pub fn new() -> QuestPNJ {
        QuestPNJ {
            name: "".to_string(),
            id: "".to_string(),
            gid: "".to_string(),
            asset: "".to_string(),
            align: Align::default(),
            place: "".to_string(),
        }
    }

    pub fn from_str(input: &str) -> PjnQuests {
        let mut reader = Reader::from_str(input);
        let mut results = PjnQuests {pnjs: HashMap::new()};
        loop {
            match reader.read_event() {
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                Ok(Event::Eof) => break,
                Ok(Event::Start(value)) => {
                    match value.name().as_ref() {
                        b"pnjs" => {/*root item with nothing*/}
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Ok(Event::Empty(value)) => {
                    match value.name().as_ref() {
                        b"p" => {
                            let mut pnj = QuestPNJ::default();
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"id" => { pnj.id = helpers::get_value(&attr) }
                                            b"school" => { pnj.align = helpers::get_align(&attr) }
                                            b"name" => { pnj.name = helpers::get_value(&attr) }
                                            b"gid" => { pnj.gid = helpers::get_value(&attr) }
                                            b"zone" => { pnj.place = helpers::get_value(&attr) }
                                            any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?}", e) }
                                }
                            });
                            results.pnjs.insert(pnj.id.to_owned(), pnj);
                        }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                _ => {}
            };
        }
        println!("Map parse ended");
        results
    }
}

