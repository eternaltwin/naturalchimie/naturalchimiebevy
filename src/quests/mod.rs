use bevy::log::info;
use bevy::prelude::*;
use crate::app_state::AppState;
use crate::common::xml_asset::{XmlAsset, XmlAssetLoader};
use crate::common_ui_sprite::{WorldSprite, WorldSpriteComponent};
use crate::menu::ui::cleanup_menu;
use crate::places::places::Places;
use crate::quests::action::response_interaction_handler;
use crate::quests::dialogs::{Dialogs, load_dialogs_assets, XmlDialog};
use crate::quests::ui_dialogs::{cleanup_quest, display_quest, CurrentDialog};
use crate::quests::pnjs::PjnQuests;
use crate::quests::quests::{load_quests_assets, Quests, XmlQuest};
use crate::quests::state::{PnjInteractionState};
use crate::quests::ui_quest::{create_quest_button, load_quest_page_interface, QuestState};
use crate::world::state::WorldState;

pub mod pnjs;
pub mod quests;
pub mod dialogs;
mod conditions;
pub mod state;
pub mod ui_dialogs;
mod action;
pub mod QObjet;
mod ui_quest;

pub struct QuestPlugin;

impl Plugin for QuestPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(PnjInteractionState::Init), setup);
        app.add_systems(Update, (load_dialogs_and_quests).run_if(in_state(PnjInteractionState::Loading)));
        app.add_systems(Update, (create_quest_button).run_if(in_state(WorldState::Customize)));
        app.add_systems(Update, (display_quest).run_if(in_state(PnjInteractionState::Dialog)));
        app.add_systems(Update, (cleanup_quest).run_if(in_state(PnjInteractionState::End)));
        app.add_systems(Update, (response_interaction_handler).run_if(in_state(PnjInteractionState::Interactive)));
        app.add_systems(Update, (loading_page).run_if(in_state(AppState::Quest)).run_if(in_state(QuestState::Loading)));
        app.add_systems(OnExit(AppState::Quest), cleanup_quest_page);
        app.add_systems(Update, (load_quest_page_interface).run_if(in_state(AppState::Quest)).run_if(in_state(QuestState::Customize)));
        app.insert_state(PnjInteractionState::Init);
        app.insert_state(QuestState::Loading);
        app.insert_resource(CurrentDialog{ id: "".to_string(), pnj_key: "".to_string() });
        app.insert_resource(Dialogs{ all: Default::default(), in_quests: Default::default() });
        app.insert_resource(Quests{ all: Default::default(), meta: Default::default() });
    }
}

fn setup(mut commands: Commands, mut state: ResMut<NextState<PnjInteractionState>>, asset_server: Res<AssetServer>) {
    load_quests_assets(&asset_server)
        .into_iter()
        .for_each(|c|{
            commands.spawn(c);
        });
    load_dialogs_assets(&asset_server)
        .into_iter()
        .for_each(|c|{
            commands.spawn(c);
        });
    state.set(PnjInteractionState::Loading);
    info!("dialogs loading");
}

fn load_dialogs_and_quests(
    mut commands: Commands,
    mut state: ResMut<NextState<PnjInteractionState>>,
    mut dialogs: ResMut<Dialogs>,
    mut quests: ResMut<Quests>,
    xml_dialogs: Query<(Entity, &XmlDialog)>,
    xml_quests: Query<(Entity, &XmlQuest)>,
    assets_loader: Res<Assets<XmlAsset>>,
)
{
    let mut remaining_elements = 0;

    for (entity,xml) in &xml_dialogs {
        let asset = assets_loader.get(xml.value());
        if let Some(a) = asset {
            if xml.is_quest_dialog() {
                dialogs.add_quest_dialog_from_str(&a.0, &xml.0.1)
            } else {
                dialogs.add_from_str(&a.0);
            }
            commands.entity(entity).despawn();
        } else {
            remaining_elements += 1;
        }
    }

    for (entity,xml) in &xml_quests {
        let asset = assets_loader.get(xml.value());
        if asset.is_none() {
            println!("look at quest :waiting");
            remaining_elements += 1;
        } else {
            println!("look at quest :parse");
            quests.add_from_str(&asset.unwrap().0);
            commands.entity(entity).despawn();
        }
    }

    dialogs.all.iter().for_each(|(key,vec)|{
        println!("{:?} have {:?} dialog(s)", key, vec.len());
    });
    println!("Dialog and quest remaining elements: {}", remaining_elements);
    if remaining_elements == 0 {
        state.set(PnjInteractionState::Ready);
    }
}

fn loading_page(mut commands: Commands, mut page_state: ResMut<NextState<QuestState>>, assets: Res<Assets<Image>>, sprites: Res<WorldSprite>) {
    println!("Loading quest page");
    let handles = sprites.get_all_handles();
    let loaded = handles.iter().fold(0, |mut acc, handle| {
        if assets.get(handle).is_some() {
            acc = acc + 1;
        }
        acc
    });
    info!("loading {}/{}", loaded,handles.len());
    if loaded == handles.len() {
        println!("menu loaded");
        sprites.load_world_ui(&mut commands);
        page_state.set(QuestState::Customize);
    }
}

fn cleanup_quest_page(mut commands: Commands, mut page_state: ResMut<NextState<QuestState>>,  sprite: Query<Entity, With<WorldSpriteComponent>>) {
    page_state.set(QuestState::Loading);
    for e in sprite.iter() {
        commands.entity(e).despawn_recursive();
    }
}