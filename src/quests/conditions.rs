use bevy::prelude::info;
use bevy::reflect::Map;
use crate::common::element::Element;
use crate::common::inventory::Inventory;
use crate::common::parse_helpers::helpers;
use crate::common::player::Player;
use crate::places::model::Align;
use crate::quests::QObjet::QObject;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum  Condition {
    Not(Box<Condition>),
    OneOf(Vec<Condition>),
    All(Vec<Condition>),
    None,
    Quest(String),
    CurQuest(String),
    School(Align),
    Reput(Align, i8),
    Effect(String),
    FirstPlayDone,
    Demo,
    HasQObject(QObject),
    Grade(Align, u8),
}

impl Condition {
    pub fn is_valid(&self, player: &Player, inventory: &Inventory ) -> bool {
        match self {
            Condition::Not(c) => {!c.is_valid(player, inventory)}
            Condition::OneOf(cs) => { cs.iter().find(|c|c.is_valid(player, inventory)).is_some() }
            Condition::All(cs) => { !cs.iter().find(|c| !c.is_valid(player, inventory)).is_some() }
            Condition::None => { true }
            Condition::Quest(c) => { player.have_done(c)}
            Condition::CurQuest(c) => { player.doing(c)}
            Condition::School(c) => { &player.school == c}
            Condition::Reput(school, value) => { player.have_reput(school, *value) }
            Condition::Effect(c) => { player.have_effect(c)}
            Condition::FirstPlayDone => {player.first_play_done}
            Condition::Demo => {player.effects.contains(&"demo".to_string())}
            Condition::HasQObject(QObject::Elt(idx, count)) => {
                println!("HasQObject  {:?} ? {:?} >=  {:?}",idx,  inventory.elements[idx.as_index()], *count );
                inventory.elements[idx.as_index()] >= *count as u32
            }
            Condition::HasQObject(QObject::Item(_, _)) => {false}
            Condition::HasQObject(QObject::Quest(_, _)) => {false}
            Condition::HasQObject(QObject::Unparsed)=> { false}
            Condition::Grade(_, _)=> { false}
        }
    }

    pub fn from_str(input: &str) -> Self {
        if input.is_empty() {
            Condition::None
        } else {
            let (cond, rest)= Condition::extract_from_str(input,false);
            if rest.is_empty() {
                cond
            } else {
                Condition::None
            }
        }
    }

    fn extract_from_str(input: &str, unary: bool) -> (Self, &str) {
        let mut condition = Condition::None;
        let mut rest = "";
         if input.starts_with('(') {
             let (first, r) = Condition::extract_from_str(&input[1..], false);
             condition= first;
             rest = &r[1..];
         } else if input.starts_with('!') {
             let (c, r ) =  Condition::extract_from_str(&input[1..], true);
             condition= Condition::Not(Box::new(c));
             rest = r;
         } else {
             let input_cursor = input.split_once(')').unwrap();
             let parts = input_cursor.0.split('(').collect::<Vec<&str>>();
             rest = input_cursor.1;
             println!("parts {:?}, rest {:?}", parts, rest);
             let param = &parts[1][0..parts[1].len()];
             condition = match parts[0] {
                 "fx" => Condition::Effect(param.to_string()),
                 "firstplaydone" => Condition::FirstPlayDone,
                 "demo" => Condition::Demo,
                 "reput" => {
                     let params =  param.split(',').collect::<Vec<&str>>();
                     Condition::Reput(helpers::get_align_from_string(params[0]), params[1].parse().unwrap_or(0) )
                 },
                 "sch" => Condition::School(helpers::get_align_from_string(param)),
                 "quest" => Condition::Quest(param.to_string()),
                 "curquest" => Condition::CurQuest(param.to_string()),
                 "hasqobject" => {
                     println!("hasqobject {:?} ", parts);
                     match parts[1] {
                         "Elt" => {
                             let rest_cursor = rest.split_once(')').unwrap();
                             let idx = parts.last().unwrap().parse::<i32>().unwrap();
                             let count = rest_cursor.0[1..].parse::<u8>().unwrap();
                             rest = rest_cursor.1;
                             Condition::HasQObject(QObject::Elt(Element::from_i32(idx), count))
                         }
                         _ => { Condition::None }
                     }
                 }
                 "grade" => {
                     println!("grade {:?} ", parts);
                     let mut it = parts.last().unwrap().split(',');
                     let align = helpers::get_align_from_string(it.next().unwrap());
                     let count = it.next().unwrap().parse::<u8>().unwrap();
                     Condition::Grade(align, count)
                 }
                 a => {
                     info!("{:?} not handled yet", a);
                     Condition::None
                 }
             };
             println!("condition {:?}", condition);
         }
         if rest.is_empty() || unary {
             (condition, rest)
         } else {
             if rest.starts_with('+') {
                 let mut v = vec![condition];
                 let (cond, current) = Condition::extract_from_str(&rest[1..], false);
                 v.push(cond);
                 (Condition::All(v), current)
             } else if rest.starts_with('|') {
                 let mut v = vec![condition];
                 let (cond, current) = Condition::extract_from_str(&rest[1..], false);
                 v.push(cond);
                 (Condition::OneOf(v), current)
             } else if rest.starts_with('!') {
                 let (c,i ) =  Condition::extract_from_str(&rest[1..], false);
                 (Condition::Not(Box::new(c)), i)
             } else {
                 (condition, rest)
             }
         }
    }
}

#[cfg(test)]
mod test {
    use crate::places::model::Align;
    use crate::quests::conditions::Condition;
    use crate::quests::QObjet::QObject;

    #[test]
    fn parse_fx() {
        let expected = Condition::Effect("x".to_string());
        let result = Condition::from_str("fx(x)");
        assert_eq!(result, expected);
    }

    #[test]
    fn parse_not_fx() {
        let expected = Condition::Not(Box::new(Condition::Effect("x".to_string())));
        let result = Condition::from_str("!fx(x)");
        assert_eq!(result, expected);
    }

    #[test]
    fn parse_not_fx_and_sch() {
        let expected = Condition::All(vec![
            Condition::Not(Box::new(Condition::Effect("x".to_string()))),
            Condition::School(Align::Jeezara)
        ]);
        let result = Condition::from_str("!fx(x)+sch(jz)");
        assert_eq!(result, expected);
    }

    #[test]
    fn parse_not_fx_and_or_sch_reput() {
        let expected = Condition::All(vec![
            Condition::Not(Box::new(Condition::Effect("x".to_string()))),
            Condition::OneOf(vec![
                Condition::School(Align::Jeezara),
                Condition::Reput(Align::Jeezara, 2)
            ])
        ]);
        let result = Condition::from_str("!fx(x)+(sch(jz)|reput(jz,2))");
        assert_eq!(result, expected);
    }

    #[test]
    fn parse_has_quest_element() {
        let expected = Condition::All(vec![
            Condition::HasQObject(QObject::Elt(18, 3)),
            Condition::HasQObject(QObject::Elt(12, 5)),
        ]);
        let result = Condition::from_str("hasqobject(Elt(18):3)+hasqobject(Elt(12):5)");
        assert_eq!(result, expected);
    }

    #[test]
    fn parse_firstplaydone_an_effect() {
        let expected = Condition::All(vec![
            Condition::Not(Box::new(Condition::Effect("welcom".to_string()))),
            Condition::All(vec![Condition::Not(Box::new(Condition::FirstPlayDone)),
            Condition::Effect("goplay".to_string())])
        ]);
        let result = Condition::from_str("!fx(welcom)+!firstplaydone()+fx(goplay)");
        assert_eq!(result, expected);
    }
}