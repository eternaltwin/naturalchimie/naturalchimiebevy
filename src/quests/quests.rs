use std::str::from_utf8;
use bevy::asset::{AssetServer, Handle};
use bevy::prelude::{Component, Reflect, Resource};
use bevy::utils::HashMap;
use quick_xml::events::{BytesStart, Event};
use quick_xml::reader::Reader;
use crate::common::element::Element;
use crate::common::item::PlayableItem;
use crate::common::parse_helpers::helpers;
use crate::common::xml_asset::XmlAsset;
use crate::places::model::Align;
use crate::quests::conditions::Condition;
use crate::quests::dialogs::{Dialog, Dialogs};
use crate::quests::QObjet::QObject;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Quest {
    pub pnj: String,
    pub id: String,
    pub name: String,
    pub descr: String,
    pub talk: Option<QSTalk>,
    pub cond: Condition,
    pub steps: Vec<QuestStep>,
    pub priority: usize,
}

impl Quest {
    pub fn get_quest_dialog(&self) -> Option<&String> {
        self.talk.as_ref().map(|t| &t.dialog)
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct QSTalk {
    pub dialog: String,
    pub title: Option<String>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct QSCreate {
    pub elements: Vec<(Element, u8)>,
    pub all_in_one: bool,
    pub title: String,
    pub zone: Option<String>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct QSUse {
    pub elements: Vec<QObject>,
    pub qgive: Vec<QObject>,
    pub action: String,
    pub text: String,
    pub zone: String,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct QSCauldron {
    pub recipe: String,
    pub title: String,
    pub collect: Option<i32>,
    pub quantity: Option<i32>,
    pub add: Option<i32>,
    pub drop: Option<i32>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct QSPlayMod {
    pub recipe: String,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct QSMessage {
    pub zone: String,
    pub action: String,
    pub text: String,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct QSEnd {
    pub auto: bool,
    pub zone: String,
    pub text: String,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum QuestStep {
    Talk(QSTalk),
    Create(QSCreate),
    Use(QSUse),
    Cauldron(QSCauldron),
    Gold(i32),
    XP(i32),
    Reput(Align, i32),
    Element((Element, u8)),
    Item((PlayableItem, u8)),
    PlayMod(QSPlayMod),
    End(Dialog),
    Msg(QSMessage),
}

impl QuestStep {
    pub fn as_dialog(&self) -> Option<&Dialog> {
        match self {
            QuestStep::End(d) => Some(d),
            _ => None,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum GivableItem {
    element(Element)
}

impl Quest {
    pub fn new() -> Quest {
        Quest {
            pnj: "".to_string(),
            id: "".to_string(),
            name: "".to_string(),
            descr: "".to_string(),
            talk: None,
            cond: Condition::None,
            steps: Vec::new(),
            priority: 0,
        }
    }

    pub fn get_step(&self, idx: usize) -> Option<&QuestStep> {
        if idx == 0 {
            None
        } else {
            self.steps.get(idx - 1)
        }
    }

    pub fn get_step_mut(&mut self, idx: usize) -> Option<&mut QuestStep> {
        if idx == 0 {
            None
        } else {
            self.steps.get_mut(idx - 1)
        }
    }
}

#[derive(Debug)]
pub struct PnjMeta {
    pub id: String,
    pub align: Align,
    pub name: String,
    pub gid: String,
    pub location: String,
}

impl PnjMeta {
    fn new() -> Self {
        Self {
            id: "".to_string(),
            align: Default::default(),
            name: "".to_string(),
            gid: "".to_string(),
            location: "".to_string(),
        }
    }
}

#[derive(Resource)]
pub struct Quests {
    pub all: HashMap<String, Quest>,
    pub meta: HashMap<String, PnjMeta>,
}

enum Operation {
    None,
    Begin,
    End,
}

impl Quests {
    pub fn add_from_str(&mut self, input: &str) {
        let mut reader = Reader::from_str(input);
        let mut quest = Quest::new();
        let mut steps_parsing = false;
        let mut current_op = Operation::None;
        let mut quest_priority: usize =  0;
        loop {
            match reader.read_event() {
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                Ok(Event::Eof) => break,
                Ok(Event::Start(value)) => {
                    match value.name().as_ref() {
                        b"quests" => { /*root item with nothing*/ }
                        b"pnjs" => { /*root item with nothing*/ }
                        b"q" => {
                            Self::fill_quest(&mut quest, value);
                        }
                        b"create" => {
                            Self::add_create_step(&mut quest, value);
                        }
                        b"begin" => {
                            current_op = Operation::Begin;
                        }
                        b"end" => {
                            current_op = Operation::End;
                        }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Ok(Event::Empty(value)) => {
                    match value.name().as_ref() {
                        b"cauldron" => {
                            Self::add_cauldron_step(&mut quest, &value);
                        }
                        b"create" => {
                            Self::add_create_step(&mut quest, value);
                        }
                        b"gold" => {
                            Self::add_gold_step(&mut quest, value);
                        }
                        b"xp" => {
                            Self::add_xp_step(&mut quest, value);
                        }
                        b"reput" => {
                            Self::add_reput_step(&mut quest, value);
                        }
                        b"item" => {
                            Self::add_item_or_element_step(&mut quest, value);
                        }
                        b"use" => {
                            Self::add_use_step(&mut quest, value);
                        }
                        b"talk" => {
                            steps_parsing = Self::add_talk(&mut quest, value, steps_parsing);
                        }
                        b"p" => {
                            println!("p load pnj");
                            let mut pnj = PnjMeta::new();
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"id" => { pnj.id = helpers::get_value(&attr) }
                                            b"school" => { pnj.align = helpers::get_align(&attr) }
                                            b"name" => { pnj.name = helpers::get_value(&attr) }
                                            b"gid" => { pnj.gid = helpers::get_value(&attr) }
                                            b"zone" => { pnj.location = helpers::get_value(&attr) }
                                            any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?}", e) }
                                }
                            });
                            self.meta.insert(pnj.id.clone(), pnj);
                        }
                        any => println!("unparsed Empty key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Ok(Event::Text(e)) => {
                    match current_op {
                        Operation::Begin => {
                            quest.descr = from_utf8(e.into_inner().as_ref()).unwrap_or("").to_string();
                        }
                        Operation::End => {
                            let text = from_utf8(e.into_inner().as_ref()).unwrap_or("").to_string();
                            let dialog = Dialogs::new_end_quest_dialog(text);
                            quest.steps.push(QuestStep::End(dialog));
                        }
                        _ => {
                            //for debug purpose
                            //println!("unparsed {:?} : {:?}", current, from_utf8(e.into_inner().as_ref()).unwrap_or("").to_string())
                        }
                    };
                    current_op = Operation::None;
                }
                Ok(Event::End(value)) => {
                    match value.name().as_ref() {
                        b"q" => {
                            quest.priority = quest_priority;
                            quest_priority = 1 + quest_priority;
                            self.all.insert(quest.id.to_owned(), quest);
                            quest = Quest::new();
                        }
                        b"q_skip" => {
                            quest = Quest::new();
                        }
                        b"begin" => {
                            current_op = Operation::None;
                        }
                        _ => {}
                    }
                }
                _ => {}
            };
        }
        println!("Quests parse ended, quests : {:?}, {:?}", self.all.len(), self.all.keys());
    }

    fn add_talk(quest: &mut Quest, value: BytesStart, mut steps_parsing: bool) -> bool {
        let mut step = QSTalk { dialog: "".to_string(), title: None };
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"did" => { step.dialog = helpers::get_value(&attr) }
                        b"title" => { step.title = Some(helpers::get_value(&attr)) }
                        b"step" => { steps_parsing = true }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
        if steps_parsing {
            quest.steps.push(QuestStep::Talk(step));
        } else {
            quest.talk = Some(step);
        };
        steps_parsing
    }

    fn add_reput_step(quest: &mut Quest, value: BytesStart) {
        let mut count = 0;
        let mut guild = Align::None;
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"v" => {
                            count = helpers::get_numerical_value(&attr);
                        }
                        b"s" => {
                            guild = helpers::get_align(&attr);
                        }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
        quest.steps.push(QuestStep::Reput(guild, count));
    }

    fn add_xp_step(quest: &mut Quest, value: BytesStart) {
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"v" => {
                            let value = helpers::get_numerical_value(&attr);
                            quest.steps.push(QuestStep::XP(value));
                        }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
    }

    fn add_gold_step(quest: &mut Quest, value: BytesStart) {
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"v" => {
                            let value = helpers::get_numerical_value(&attr);
                            quest.steps.push(QuestStep::Gold(value));
                        }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
    }

    fn add_item_or_element_step(quest: &mut Quest, value: BytesStart) {
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"v" => {
                            let value = helpers::get_value_as_str(&attr);
                            let step = if value.starts_with("Elt") {
                                QuestStep::Element(helpers::extract_element_from_str(value))
                            } else {
                                QuestStep::Item(helpers::extract_item_from_str(value))
                            };
                            quest.steps.push(step);
                        }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
    }

    fn add_create_step(quest: &mut Quest, value: BytesStart) {
        let mut create = QSCreate {
            elements: Default::default(),
            zone: None,
            all_in_one: false,
            title: "".to_string(),
        };
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"o" => { create.elements = helpers::get_elements(&attr) }
                        b"title" => { create.title = helpers::get_value(&attr) }
                        b"zone" => { create.zone = Some(helpers::get_value(&attr)) }
                        b"allInOne" => { create.all_in_one = helpers::get_boolean_value(&attr) }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
        let step = QuestStep::Create(create);
        quest.steps.push(step);
    }


    fn add_use_step(quest: &mut Quest, value: BytesStart) {
        let mut step = QSUse {
            elements: vec![],
            qgive: vec![],
            action: "".to_string(),
            text: "".to_string(),
            zone: "".to_string(),
        };
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"o" => { step.elements = helpers::get_quests_objects(&attr) }
                        b"text" => { step.text = helpers::get_value(&attr) }
                        b"action" => { step.action = helpers::get_value(&attr) }
                        b"zone" => { step.zone = helpers::get_value(&attr) }
                        b"qgive" => { step.qgive = helpers::get_quests_objects(&attr) }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
        let step = QuestStep::Use(step);
        quest.steps.push(step);
    }

    fn add_cauldron_step(quest: &mut Quest, value: &BytesStart) {
        let mut cauldron = QSCauldron { recipe: "".to_string(), title: "".to_string(), collect: None, quantity: None, add: None, drop: None };
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"r" => { cauldron.recipe = helpers::get_value(&attr) }
                        b"title" => { cauldron.title = helpers::get_value(&attr) }
                        b"collect" => { cauldron.collect = Some(helpers::get_numerical_value(&attr)) }
                        b"qty" => { cauldron.quantity = Some(helpers::get_numerical_value(&attr)) }
                        b"add" => { cauldron.add = Some(helpers::get_numerical_value(&attr)) }
                        b"drop" => { cauldron.drop = Some(helpers::get_numerical_value(&attr)) }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
        let step = QuestStep::Cauldron(cauldron);
        quest.steps.push(step);
    }

    fn fill_quest(mut quest: &mut Quest, value: BytesStart) {
        value.attributes().for_each(|maybe| {
            match maybe {
                Ok(attr) => {
                    match attr.key.as_ref() {
                        b"from" => { quest.pnj = helpers::get_value(&attr) }
                        b"id" => { quest.id = helpers::get_value(&attr) }
                        b"cond" => { quest.cond = Condition::from_str(helpers::get_value_as_str(&attr)) }
                        b"name" => { quest.name = helpers::get_value(&attr) }
                        any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Err(e) => { println!("error {:?}", e) }
            }
        });
    }
}

#[derive(Component)]
pub(crate) struct XmlQuest(Handle<XmlAsset>);

impl XmlQuest {
    pub(crate) fn value<'a>(&'a self) -> &'a Handle<XmlAsset> {
        &self.0
    }
}

//TODO see if :
// - we can juste read directory with web version ?
//  or
// - we move all dialogs in one file ?
pub fn load_quests_assets(asset_server: &AssetServer) -> Vec<XmlQuest> {
    [
        "texts/questPnjs.xml",
        "texts/quests/altus.xml",
        "texts/quests/bafa.xml",
        "texts/quests/biactol.xml",
        "texts/quests/borgne.xml",
        "texts/quests/lapin.xml",
        "texts/quests/liborgne.xml",
        "texts/quests/link.xml",
        "texts/quests/major.xml",
        "texts/quests/melch.xml",
        "texts/quests/mite.xml",
        "texts/quests/pirlotte.xml",
        "texts/quests/poum.xml",
        "texts/quests/prairiebhv.xml",
        "texts/quests/prairiegub.xml",
        "texts/quests/prairiegu.xml",
        "texts/quests/pumpkin.xml",
        "texts/quests/schap.xml",
        "texts/quests/schgmb.xml",
        "texts/quests/schgm.xml",
        "texts/quests/schjz.xml",
        "texts/quests/schskb.xml",
        "texts/quests/schsk.xml",
        "texts/quests/trifbg.xml",
        "texts/quests/vorace.xml",
        "texts/quests/ylasse.xml",
    ]
        .iter()
        .map(|path| {
            XmlQuest(asset_server.load(path.to_owned()))
        }).collect()
}

