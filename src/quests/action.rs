use bevy::log::info;
use bevy::prelude::{Button, Changed, Component, Interaction, NextState, Query, ResMut, With};
use crate::app_state::AppState;
use crate::common::player::Player;
use crate::menu::state::MenuState;
use crate::quests::dialogs::QuestAction;
use crate::quests::quests::{Quest, QuestStep};
use crate::quests::ui_dialogs::CurrentDialog;
use crate::quests::state::PnjInteractionState;

#[derive(Component, Debug)]
pub enum DialogButtonAction {
    Response {value: String, effect: String},
    Quest {value: Quest},
    End { value: QuestAction, effect: String},
    ListQuest,
    ReturnWorld,
    None,
}

pub fn response_interaction_handler(
    interaction_query: Query<
        (&Interaction, &DialogButtonAction),
        (Changed<Interaction>, With<Button>),
    >,
    mut current_dialog: ResMut<CurrentDialog>,
    mut quests_state: ResMut<NextState<PnjInteractionState>>,
    mut app_state: ResMut<NextState<AppState>>,
    mut menu_state: ResMut<NextState<MenuState>>,
    mut player: ResMut<Player>,
) {
    for (interaction, button_action) in &interaction_query {
        if *interaction == Interaction::Pressed {
            match button_action {
                DialogButtonAction::Response { value, effect} => {
                    println!("Response");
                    quests_state.set(PnjInteractionState::Dialog);;
                    player.effects.push(effect.clone());
                    current_dialog.pnj_key = value.to_string();
                }
                DialogButtonAction::End { value, effect} => {
                    println!("End");
                    quests_state.set(PnjInteractionState::End);
                    app_state.set(AppState::RedrawWorld);
                    player.effects.push(effect.clone());
                    match &player.active_quest {
                        Some((quest, step)) => {
                            if step == &0 {
                                player.incr_step();
                            } else {
                                match quest.steps.get(step - 1) {
                                    Some(QuestStep::End(_)) => {
                                        let string = quest.id.to_string();
                                        player.quest_status.insert(string, true);
                                        player.active_quest = None;
                                    }
                                    None => { println!("debug on end quest {:?}, step {}", quest, step); }
                                    _ => { player.incr_step(); }
                                }
                            }
                        }
                        None => {}
                    }
                    exec_action(&mut app_state, &mut menu_state, value);
                }
                DialogButtonAction::ListQuest => {
                    println!("ListQuest");
                    exec_action(&mut app_state, &mut menu_state, &QuestAction::GoToQuestPage);
                }
                DialogButtonAction::Quest { value} => {
                    println!("Quest {:?}", value);
                    match &value.talk {
                        Some(t) => {
                            quests_state.set(PnjInteractionState::Dialog);
                            current_dialog.id = t.dialog.to_string();
                            current_dialog.pnj_key = "begin".to_string();
                            player.active_quest = Some((value.clone(), 0));
                            exec_action(&mut app_state, &mut menu_state, &QuestAction::ReturnWorld);
                        }
                        _ => {}
                    }
                }
                DialogButtonAction::ReturnWorld => {
                    println!("ReturnWorld");
                    exec_action(&mut app_state, &mut menu_state, &QuestAction::ReturnWorld);
                }
                act => {
                    info!("dialog {:?} interaction not implemented", act)
                }
            }
        }
    }
}

fn exec_action(app_state: &mut ResMut<NextState<AppState>>, menu_state: &mut ResMut<NextState<MenuState>>, action: &QuestAction) {
    match action {
        QuestAction::None => {}
        QuestAction::Play => {
            app_state.set(AppState::Game); // add here the custom element ?
            menu_state.set(MenuState::None);
        }
        QuestAction::Act => {}
        QuestAction::QuitSchool => {}
        QuestAction::Subscribe => {}
        QuestAction::ReturnWorld => {
            app_state.set(AppState::World);
            menu_state.set(MenuState::None);
        }
        QuestAction::GoToQuestPage => {
            app_state.set(AppState::Quest);
            menu_state.set(MenuState::None);
        }
    }
}
