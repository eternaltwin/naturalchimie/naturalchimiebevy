use std::ops::Deref;
use std::str::from_utf8;
use bevy::asset::{AssetServer, Handle};
use bevy::prelude::{Component, Resource};
use bevy::utils::HashMap;
use quick_xml::events::Event;
use quick_xml::reader::Reader;
use crate::common::inventory::Inventory;
use crate::common::parse_helpers::helpers;
use crate::common::player::Player;
use crate::common::xml_asset::XmlAsset;
use crate::quests::conditions::Condition;
use crate::quests::QObjet::QObject;
use crate::quests::quests::{PnjMeta, QuestStep};

#[derive(Resource)]
pub struct Dialogs {
    pub all: HashMap<String, Vec<Dialog>>,
    pub in_quests: HashMap<String, Dialog>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum QuestAction {
    None,
    Play,
    Act,
    QuitSchool,
    Subscribe,
    GoToQuestPage,
    ReturnWorld
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Dialog {
    pub place: String,
    pub name: String,
    pub from: String,
    pub(crate) auto: bool,
    condition: Condition,
    pub pnj: HashMap<String, Request>,
    pub player: HashMap<String, Response>,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Request {
    pub id: String,
    pub frame: String,
    pub next: Vec<String>,
    pub text: String,
    pub effect: String,
    pub quest: Option<String>,
    pub action: QuestAction,
    pub give: QObject, //"Pa:6"
    pub gold: i32,
    pub collection: String,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Response {
    pub id: String,
    pub condition: Condition,
    pub text: String,
}

enum Operation {
    Phase(Request),
    A(Response),
    Dialog,
}

impl Dialogs {
    pub fn new_end_quest_dialog(text: String) -> Dialog{
        let mut dialog = Dialog {
            place: "".to_string(),
            name: "".to_string(),
            from: "".to_string(),
            auto: true,
            condition: Condition::None,
            pnj: Default::default(),
            player: Default::default(),
        };
        let mut request = Dialogs::new_request();
        request.text= text;
        request.id= "begin".to_string();
        dialog.pnj.insert("begin".to_string(), request );
        dialog
    }

    pub fn new_request() -> Request {
        Request {
            id: "".to_string(),
            frame: "".to_string(),
            next: Vec::new(),
            text: "".to_string(),
            effect: "".to_string(),
            quest: None,
            action: QuestAction::None,
            give: QObject::Unparsed,
            gold: 0,
            collection: "".to_string(),
        }
    }

    pub fn new_response() -> Response {
        Response {
            id: "".to_string(),
            condition: Condition::None,
            text: "".to_string(),
        }
    }

    pub fn get_dialogs_for<'a>(&'a self, player: &'a Player, meta: &HashMap<String, PnjMeta>,inventory: &Inventory) -> Vec<&'a Dialog> {
        println!("player quest ? {:?}", player.active_quest);
        if let Some((quest, step)) = player.active_quest.as_ref() {
            if *step == 0 {
                if meta.get(&quest.pnj).map(|meta| meta.location == player.location).unwrap_or(false) {
                    match self.in_quests.get(quest.get_quest_dialog().unwrap()) {
                        None => {
                            println!("fallback on generic dialogs 1");
                            self.get_generic_dialogs(&player, inventory)
                        }
                        Some(d) => {
                            println!("found {}", d.name);
                            vec![d]
                        }
                    }
                } else {
                    self.get_generic_dialogs(&player, inventory)
                }
            } else {
                match &quest.steps[step - 1 ] {
                    QuestStep::Talk(talk) => {
                        println!("search for quest dialogs {} in {:?}", &talk.dialog, self.in_quests.keys().collect::<Vec<&String>>());
                        match self.in_quests.get(&talk.dialog) {
                            None => {
                                println!("fallback on generic dialogs 1");
                                self.get_generic_dialogs(&player, inventory)
                            }
                            Some(d) => { vec![d] }
                        }
                    }
                    QuestStep::End(content) => {
                        println!("End quest {:?} {:?} {:?}", quest.name, quest.pnj,  content);
                        println!("meta {:?}", meta);
                        println!("find pnj ? {:?} {:?}", meta.get(&quest.pnj) , meta.get(&quest.pnj).map(|meta| meta.location == player.location).unwrap_or(false));
                        if meta.get(&quest.pnj).map(|meta| meta.location == player.location).unwrap_or(false) {
                            vec![content]
                        } else {
                            self.get_generic_dialogs(&player, inventory)
                        }
                    }
                    step => {
                        println!("fallback on generic dialogs 2 with step {:?}", step);
                        self.get_generic_dialogs(&player, inventory)
                    }
                }
            }
        } else {
            self.get_generic_dialogs(&player, inventory)
        }
    }

    fn get_generic_dialogs(&self, player: &&Player, inventory: &Inventory) -> Vec<&Dialog> {
        self
            .all
            .get(&player.location)
            .map_or(Vec::new(), |dialogs| {
                println!("select in Dialogs:");
                dialogs
                    .into_iter()
                    .filter(|dialog| {
                        dialog.condition.is_valid(player, inventory)
                    })
                    .collect()
            })
    }

    pub fn add_from_str(&mut self, input: &str) {
        let result = Self::extract_from_str(input);
        let values = self.all.entry(result.place.to_owned()).or_default();
        values.push(result);
    }

    pub fn add_quest_dialog_from_str(&mut self, input: &str, path: &str) {
        let result = Self::extract_from_str(input);
        let key = path.split('/').last().unwrap().split('.').next().unwrap();
        println!("1add quest dialog from str: {} as id {} ", path, key);
        self.in_quests.insert(key.to_owned(), result.clone());
    }

    fn extract_from_str(input: &str) -> Dialog {
        let mut reader = Reader::from_str(input);
        let mut result = Dialog { place: "".to_string(), name: "".to_string(), from: "".to_string(), auto: false, condition: Condition::None, pnj: HashMap::new(), player: HashMap::new() };

        let mut operation = None;

        loop {
            match reader.read_event() {
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                Ok(Event::Eof) => break,
                Ok(Event::Start(value)) => {
                    match value.name().as_ref() {
                        b"d" => {
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"name" => { result.name = helpers::get_value(&attr) }
                                            b"place" => { result.place = helpers::get_value(&attr) }
                                            b"from" => { result.from = helpers::get_value(&attr) }
                                            //b"frame" => { result.frame = helpers::get_value(&attr) }
                                            b"auto" => { result.auto = helpers::get_boolean_value(&attr) }
                                            b"cond" => { result.condition = Condition::from_str(helpers::get_value_as_str(&attr)) }
                                            any => println!("unparsed key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?} on parsing dialog {:?}", e, result.name) }
                                }
                            });
                            operation = Some(Operation::Dialog)
                        }
                        b"phase" => {
                            let mut request = Dialogs::new_request();
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"id" => { request.id = helpers::get_value(&attr) }
                                            b"frame" => { request.frame = helpers::get_value(&attr) }
                                            b"quest" => { request.quest = Some(helpers::get_value(&attr)) }
                                            b"next" => { request.next = (&helpers::get_value(&attr)).split(':').map(|t| t.to_string()).collect() }
                                            b"effect" => { request.effect = helpers::get_value(&attr) }
                                            b"give" => { request.effect = helpers::get_value(&attr) }
                                            b"gold" => { request.gold = helpers::get_numerical_value(&attr) }
                                            b"collection" => { request.collection = helpers::get_value(&attr) }
                                            b"url" => {
                                                request.action = match helpers::get_value(&attr).as_str() {
                                                    "/act/play" => { QuestAction::Play }
                                                    "/act" => { QuestAction::Act }
                                                    any => {
                                                        println!("unparsed request action {}", any);
                                                        QuestAction::None
                                                    }
                                                }
                                            }
                                            any => println!("unparsed request key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?}", e) }
                                }
                            });
                            operation = Some(Operation::Phase(request))
                        }
                        b"a" => {
                            let mut response = Dialogs::new_response();
                            value.attributes().for_each(|maybe| {
                                match maybe {
                                    Ok(attr) => {
                                        match attr.key.as_ref() {
                                            b"id" => { response.id = helpers::get_value(&attr) }
                                            b"cond" => { response.condition = Condition::from_str(helpers::get_value_as_str(&attr)) }
                                            any => println!("unparsed response key {}", from_utf8(any).unwrap_or(""))
                                        }
                                    }
                                    Err(e) => { println!("error {:?}", e) }
                                }
                            });
                            operation = Some(Operation::A(response))
                        }
                        any => println!("unparsed dialog key {}", from_utf8(any).unwrap_or(""))
                    }
                }
                Ok(Event::Text(e)) => {
                    match &mut operation {
                        Some(Operation::Phase(r)) => {
                            r.text = from_utf8(e.into_inner().as_ref()).unwrap_or("").to_string();
                        }
                        Some(Operation::A(r)) => {
                            r.text = from_utf8(e.into_inner().as_ref()).unwrap_or("").to_string();
                        }
                        _ => {}
                    }
                }
                Ok(Event::End(_)) => {
                    match operation {
                        Some(Operation::Phase(r)) => {
                            result.pnj.insert(r.id.clone(), r);
                        }
                        Some(Operation::A(r)) => {
                            result.player.insert(r.id.clone(), r);
                        }
                        _ => {}
                    }
                    operation = None;
                }
                _ => {}
            };
        }
        println!("Dialog {} in {} parse ended", result.name, result.place);
        result
    }
}

#[derive(Component)]
pub(crate) struct XmlDialog(pub(crate) (Handle<XmlAsset>, String));

impl XmlDialog {
    pub(crate) fn value<'a>(&'a self) -> &'a Handle<XmlAsset> {
        &self.0.0
    }

    pub(crate) fn is_quest_dialog<'a>(&'a self) -> bool {
        self.0.1.starts_with("texts/dialogs/quest/")
    }
}

//TODO see if :
// - we can juste read directory with web version ?
//  or
// - we move all dialogs in one file ?
pub fn load_dialogs_assets(asset_server: &AssetServer) -> Vec<XmlDialog> {
    [
        "texts/dialogs/archeobro.xml",
        "texts/dialogs/archeonobro.xml",
        "texts/dialogs/begin0-1.xml",
        "texts/dialogs/begin0-2.xml",
        "texts/dialogs/begin0-3.xml",
        "texts/dialogs/begin0.xml",
        "texts/dialogs/borneb.xml",
        "texts/dialogs/callChx.xml",
        "texts/dialogs/callnbrq.xml",
        "texts/dialogs/callnbtp.xml",
        "texts/dialogs/cauldron/g2_dialog.xml",
        "texts/dialogs/cauldron/keeper_cthul.xml",
        "texts/dialogs/cauldron/g1_dialog.xml",
        "texts/dialogs/cauldron/tuto_1.xml",
        "texts/dialogs/cauldron/g4_dialog.xml",
        "texts/dialogs/cauldron/keeper_buy.xml",
        "texts/dialogs/cauldron/keeper_cant_buy.xml",
        "texts/dialogs/cauldron/keeper_qkevin.xml",
        "texts/dialogs/cauldron/keeper_help.xml",
        "texts/dialogs/cauldron/g3_dialog.xml",
        "texts/dialogs/cupcall.xml",
        "texts/dialogs/gfCall.xml",
        "texts/dialogs/godFather.xml",
        "texts/dialogs/gucall.xml",
        "texts/dialogs/hair.xml",
        "texts/dialogs/helpChx.xml",
        "texts/dialogs/kirvie.xml",
        "texts/dialogs/lapin.xml",
        "texts/dialogs/liborgne.xml",
        "texts/dialogs/lpcall.xml",
        "texts/dialogs/monstre.xml",
        "texts/dialogs/nbrq.xml",
        "texts/dialogs/newMsg.xml",
        "texts/dialogs/newsChx.xml",
        "texts/dialogs/no_aparmy_reput.xml",
        "texts/dialogs/nowaymonster.xml",
        "texts/dialogs/nrank.xml",
        "texts/dialogs/po2.xml",
        "texts/dialogs/poap.xml",
        "texts/dialogs/pogm.xml",
        "texts/dialogs/pojz.xml",
        "texts/dialogs/portesOuvertes.xml",
        "texts/dialogs/posk.xml",
        "texts/dialogs/qAltus.xml",
        "texts/dialogs/qaroun.xml",
        "texts/dialogs/qbafa.xml",
        "texts/dialogs/qbiactol.xml",
        "texts/dialogs/qborgne.xml",
        "texts/dialogs/qitsch.xml",
        "texts/dialogs/qjard.xml",
        "texts/dialogs/qlinkb.xml",
        "texts/dialogs/qlink.xml",
        "texts/dialogs/qmajor.xml",
        "texts/dialogs/qmelch.xml",
        "texts/dialogs/qmite.xml",
        "texts/dialogs/qpirlotte.xml",
        "texts/dialogs/qpoum.xml",
        "texts/dialogs/qprairiebhv.xml",
        "texts/dialogs/qprairiegub.xml",
        "texts/dialogs/qprairiegu.xml",
        "texts/dialogs/qpumpkintroc.xml",
        "texts/dialogs/qpumpkin.xml",
        "texts/dialogs/qrhesus.xml",
        "texts/dialogs/qschap.xml",
        "texts/dialogs/qschgmb.xml",
        "texts/dialogs/qschgm.xml",
        "texts/dialogs/qschjz.xml",
        "texts/dialogs/qschskb.xml",
        "texts/dialogs/qschsk.xml",
        "texts/dialogs/qtrib.xml",
        "texts/dialogs/qtric.xml",
        "texts/dialogs/qtri.xml",
        "texts/dialogs/quest/biacab.xml",
        "texts/dialogs/quest/lapca.xml",
        "texts/dialogs/quest/gmda.xml",
        "texts/dialogs/quest/libordb.xml",
        "texts/dialogs/quest/balafb.xml",
        "texts/dialogs/quest/poumab.xml",
        "texts/dialogs/quest/skatga.xml",
        "texts/dialogs/quest/gmib.xml",
        "texts/dialogs/quest/lapcb.xml",
        "texts/dialogs/quest/skbab.xml",
        "texts/dialogs/quest/lapfd.xml",
        "texts/dialogs/quest/boursb.xml",
        "texts/dialogs/quest/lapeb.xml",
        "texts/dialogs/quest/skbhb.xml",
        "texts/dialogs/quest/bafahb.xml",
        "texts/dialogs/quest/bornea.xml",
        "texts/dialogs/quest/skhb.xml",
        "texts/dialogs/quest/skbba.xml",
        "texts/dialogs/quest/jzha.xml",
        "texts/dialogs/quest/ylasca.xml",
        "texts/dialogs/quest/skga.xml",
        "texts/dialogs/quest/skbga.xml",
        "texts/dialogs/quest/liborea.xml",
        "texts/dialogs/quest/apkb.xml",
        "texts/dialogs/quest/skbea.xml",
        "texts/dialogs/quest/poumha.xml",
        "texts/dialogs/quest/lapfc.xml",
        "texts/dialogs/quest/gmjb.xml",
        "texts/dialogs/quest/miteaa.xml",
        "texts/dialogs/quest/biacaa.xml",
        "texts/dialogs/quest/skib.xml",
        "texts/dialogs/quest/bhvda.xml",
        "texts/dialogs/quest/jzea.xml",
        "texts/dialogs/quest/popo0.xml",
        "texts/dialogs/quest/poumaa.xml",
        "texts/dialogs/quest/boursc.xml",
        "texts/dialogs/quest/bafafb.xml",
        "texts/dialogs/quest/liboree.xml",
        "texts/dialogs/quest/skbid.xml",
        "texts/dialogs/quest/liboreb.xml",
        "texts/dialogs/quest/skbia.xml",
        "texts/dialogs/quest/skda.xml",
        "texts/dialogs/quest/boursa.xml",
        "texts/dialogs/quest/gmic.xml",
        "texts/dialogs/quest/bafafa.xml",
        "texts/dialogs/quest/bafaaa.xml",
        "texts/dialogs/quest/liborda.xml",
        "texts/dialogs/quest/melchaa.xml",
        "texts/dialogs/quest/gmga.xml",
        "texts/dialogs/quest/lapga.xml",
        "texts/dialogs/quest/melchcb.xml",
        "texts/dialogs/quest/skatca.xml",
        "texts/dialogs/quest/skatcb.xml",
        "texts/dialogs/quest/poumia.xml",
        "texts/dialogs/quest/skatfa.xml",
        "texts/dialogs/quest/lapba.xml",
        "texts/dialogs/quest/balaba.xml",
        "texts/dialogs/quest/bafaja.xml",
        "texts/dialogs/quest/balaca.xml",
        "texts/dialogs/quest/ylasaa.xml",
        "texts/dialogs/quest/liborec.xml",
        "texts/dialogs/quest/ylascb.xml",
        "texts/dialogs/quest/gmia.xml",
        "texts/dialogs/quest/jzhb.xml",
        "texts/dialogs/quest/majoria.xml",
        "texts/dialogs/quest/mitecd.xml",
        "texts/dialogs/quest/skbdb.xml",
        "texts/dialogs/quest/tricd.xml",
        "texts/dialogs/quest/jzhd.xml",
        "texts/dialogs/quest/poumfd.xml",
        "texts/dialogs/quest/lapbb.xml",
        "texts/dialogs/quest/tribc.xml",
        "texts/dialogs/quest/skatba.xml",
        "texts/dialogs/quest/bhvaa.xml",
        "texts/dialogs/quest/skca.xml",
        "texts/dialogs/quest/ylasba.xml",
        "texts/dialogs/quest/poumfb.xml",
        "texts/dialogs/quest/skatda.xml",
        "texts/dialogs/quest/lapda.xml",
        "texts/dialogs/quest/bornec.xml",
        "texts/dialogs/quest/lapfa.xml",
        "texts/dialogs/quest/skbha.xml",
        "texts/dialogs/quest/miteca.xml",
        "texts/dialogs/quest/majorba.xml",
        "texts/dialogs/quest/jzfa.xml",
        "texts/dialogs/quest/bafaha.xml",
        "texts/dialogs/quest/majorea.xml",
        "texts/dialogs/quest/melchda.xml",
        "texts/dialogs/quest/apka.xml",
        "texts/dialogs/quest/skatfb.xml",
        "texts/dialogs/quest/laphb.xml",
        "texts/dialogs/quest/skja.xml",
        "texts/dialogs/quest/poumjb.xml",
        "texts/dialogs/quest/jzhc.xml",
        "texts/dialogs/quest/triba.xml",
        "texts/dialogs/quest/bafala.xml",
        "texts/dialogs/quest/tricc.xml",
        "texts/dialogs/quest/gmha.xml",
        "texts/dialogs/quest/poumjc.xml",
        "texts/dialogs/quest/lapcc.xml",
        "texts/dialogs/quest/bhvab.xml",
        "texts/dialogs/quest/skatdc.xml",
        "texts/dialogs/quest/popo1.xml",
        "texts/dialogs/quest/majoraa.xml",
        "texts/dialogs/quest/triab.xml",
        "texts/dialogs/quest/jzhe.xml",
        "texts/dialogs/quest/poumfc.xml",
        "texts/dialogs/quest/skbib.xml",
        "texts/dialogs/quest/liborac.xml",
        "texts/dialogs/quest/bhvdb.xml",
        "texts/dialogs/quest/lapia.xml",
        "texts/dialogs/quest/jzgb.xml",
        "texts/dialogs/quest/bafaga.xml",
        "texts/dialogs/quest/poumgb.xml",
        "texts/dialogs/quest/jzda.xml",
        "texts/dialogs/quest/skatdb.xml",
        "texts/dialogs/quest/tribb.xml",
        "texts/dialogs/quest/majorga.xml",
        "texts/dialogs/quest/poumga.xml",
        "texts/dialogs/quest/poumhb.xml",
        "texts/dialogs/quest/liborab.xml",
        "texts/dialogs/quest/jzeb.xml",
        "texts/dialogs/quest/jzaa.xml",
        "texts/dialogs/quest/skgb.xml",
        "texts/dialogs/quest/trica.xml",
        "texts/dialogs/quest/jzca.xml",
        "texts/dialogs/quest/mitecc.xml",
        "texts/dialogs/quest/lapha.xml",
        "texts/dialogs/quest/jzcb.xml",
        "texts/dialogs/quest/liboraa.xml",
        "texts/dialogs/quest/lapaa.xml",
        "texts/dialogs/quest/skba.xml",
        "texts/dialogs/quest/lapdb.xml",
        "texts/dialogs/quest/skhd.xml",
        "texts/dialogs/quest/poumib.xml",
        "texts/dialogs/quest/siflea.xml",
        "texts/dialogs/quest/skhc.xml",
        "texts/dialogs/quest/triaa.xml",
        "texts/dialogs/quest/skbgb.xml",
        "texts/dialogs/quest/skbaa.xml",
        "texts/dialogs/quest/gmja.xml",
        "texts/dialogs/quest/skha.xml",
        "texts/dialogs/quest/schooa.xml",
        "texts/dialogs/quest/lapib.xml",
        "texts/dialogs/quest/sifleb.xml",
        "texts/dialogs/quest/lapea.xml",
        "texts/dialogs/quest/libored.xml",
        "texts/dialogs/quest/skatea.xml",
        "texts/dialogs/quest/lapfb.xml",
        "texts/dialogs/quest/skbhd.xml",
        "texts/dialogs/quest/bafafc.xml",
        "texts/dialogs/quest/bafaia.xml",
        "texts/dialogs/quest/gmbga.xml",
        "texts/dialogs/quest/grimb.xml",
        "texts/dialogs/quest/jzba.xml",
        "texts/dialogs/quest/liborba.xml",
        "texts/dialogs/quest/jzga.xml",
        "texts/dialogs/quest/skia.xml",
        "texts/dialogs/quest/poumfa.xml",
        "texts/dialogs/quest/bafafd.xml",
        "texts/dialogs/quest/apja.xml",
        "texts/dialogs/quest/skatbb.xml",
        "texts/dialogs/quest/grima.xml",
        "texts/dialogs/quest/gmbgb.xml",
        "texts/dialogs/quest/skbeb.xml",
        "texts/dialogs/quest/skbhc.xml",
        "texts/dialogs/quest/skbic.xml",
        "texts/dialogs/quest/triac.xml",
        "texts/dialogs/quest/mitecb.xml",
        "texts/dialogs/quest/tricb.xml",
        "texts/dialogs/quest/balafa.xml",
        "texts/dialogs/quest/melchca.xml",
        "texts/dialogs/quest/ylasbb.xml",
        "texts/dialogs/quest/gmbaa.xml",
        "texts/dialogs/quest/gmea.xml",
        "texts/dialogs/quest/poumja.xml",
        "texts/dialogs/quest/skjb.xml",
        "texts/dialogs/quest/skbda.xml",
        "texts/dialogs/quest/siflec.xml",
        "texts/dialogs/qvorace.xml",
        "texts/dialogs/qylasse.xml",
        "texts/dialogs/schoolCup.xml",
        "texts/dialogs/schsubap.xml",
        "texts/dialogs/schsubgm.xml",
        "texts/dialogs/schsubjz.xml",
        "texts/dialogs/schsubsk.xml",
        "texts/dialogs/testPnj.xml",
        "texts/dialogs/wheel.xml",
        "texts/dialogs/xmas.xml"
    ]
        .iter()
        .map(|path| {
            XmlDialog((asset_server.load(path.to_owned()),path.to_string()))
        }).collect()
}
