use bevy::prelude::{States};

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
pub enum PnjInteractionState {
    #[default]
    Init,
    Loading,
    Ready,
    End,
    Dialog,
    Interactive
}