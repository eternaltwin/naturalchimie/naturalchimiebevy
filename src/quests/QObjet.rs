use crate::common::element::Element;
use crate::common::item::PlayableItem;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum QuestObject {
    Chocapic,
    Geminishred,
    Geminishregular,
    Ink,
    Wombat,
    Teleport,
    GodFather,
    Ocarina,
    Broom2,
    GoldTrap,
    Cage,
    Livre,
    OblForm,
    Fiente,
    Corb,
    Unparsed(String),
    Michelin,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum QObject {
    Elt(Element,u8), // element index, element quantity
    Item(PlayableItem,u8), // element index, element quantity
    Quest(QuestObject,u8), // element index, element quantity
    Unparsed,
}