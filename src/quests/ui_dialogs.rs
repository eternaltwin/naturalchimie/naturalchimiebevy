use bevy::color::palettes::css::{ANTIQUE_WHITE, BLACK, GREEN};
use bevy::prelude::*;
use crate::common::inventory::Inventory;
use crate::common::player::Player;
use crate::menu::ui::MenuButton;
use crate::quests::action::DialogButtonAction;
use crate::quests::dialogs::{Dialog, Dialogs, Request};
use crate::quests::QObjet::QObject;
use crate::quests::quests::{Quest, Quests};
use crate::quests::state::PnjInteractionState;
use crate::world::{PlaceImage, PlaceText};


#[derive(Resource)]
pub struct CurrentDialog {
    pub id: String,
    pub pnj_key: String,
}

#[derive(Component)]
pub struct MapDisplayer;

pub fn cleanup_quest(
    mut commands: Commands,
    sprite: Query<Entity, With<MapDisplayer>>,
    mut quests_state: ResMut<NextState<PnjInteractionState>>,
)
{
    println!("exit Quests. remove {} entities", sprite.iter().count());
    for e in sprite.iter() {
        commands.entity(e).despawn_recursive();
    }
    quests_state.set(PnjInteractionState::Interactive);
}

pub fn display_quest(
    mut commands: Commands,
    mut quests_state: ResMut<NextState<PnjInteractionState>>,
    previous_sprite: Query<Entity, With<MapDisplayer>>,
    place_image: Query<Entity, With<PlaceImage>>,
    place_text: Query<Entity, With<PlaceText>>,
    player: Res<Player>,
    interaction: Res<CurrentDialog>,
    dialogs: Res<Dialogs>,
    quests: Res<Quests>,
    mut inventory: ResMut<Inventory>,
)
{
    for e in previous_sprite.iter() {
        commands.entity(e).despawn_recursive();
    }
    let entity_image = place_image.get_single();
    let entity_text = place_text.get_single();
    match (entity_image, entity_text) {
        (Ok(entity_image), Ok(entity_text)) => {
            println!("test {}", interaction.id);
            let place_dialogs = dialogs.get_dialogs_for(&player, &quests.meta, &inventory);
            println!("place_dialogs {:?}", place_dialogs.iter().map(|f|f.name.clone()).collect::<Vec<String>>());
            if let Some(dialog)= place_dialogs.iter().find(|d| d.name == interaction.id){
                match dialog.pnj.get(&interaction.pnj_key) {
                    Some(request) => {
                        println!("request found {:?}", request);
                        commands
                            .entity(entity_image)
                            .with_children(|parent| {
                                display_pnj(parent, request);
                            });
                        commands
                            .entity(entity_text)
                            .with_children(|parent| {
                                display_choices(parent, &quests, dialog, request, &player, &inventory);
                            });
                        if request.give != QObject::Unparsed {
                            inventory.kubor += request.gold as u32;
                            match request.give {
                                QObject::Elt(el, count) => {
                                    inventory.elements[el.as_index()] += count as u32;
                                }
                                _ => {}
                            }
                        }
                    }
                    _ => {}
                }
            }
            quests_state.set(PnjInteractionState::Interactive);
        }
        _ => { /*wait next render*/ }
    }
}

fn display_pnj(parent: &mut ChildBuilder, request: &Request)
{
    parent.spawn((
        NodeBundle {
            style: Style {
                width: Val::Percent(80.),
                padding: UiRect::all(Val::Px(10.)),
                position_type: PositionType::Absolute,
                bottom: Val::Percent(5.0),
                left: Val::Percent(0.0),
                ..default()
            },
            background_color: BackgroundColor(Srgba::new(200.0, 200.0, 200.0, 0.9).into()),
            ..default()
        },
        MapDisplayer
    ))
        .with_children(|parent| {
            println!("display pnj text {}", request.text);
            parent.spawn(TextBundle::from_section(
                request.text.to_owned(),
                TextStyle {
                    font_size: 16.0,
                    color: BLACK.into(),
                    ..default()
                },
            ));
        });
}

fn display_choices(
    parent: &mut ChildBuilder,
    quests: &Quests,
    dialog: &Dialog,
    request: &Request,
    player: &Player,
    inventory: &Inventory,
)
{
    parent.spawn((
        NodeBundle {
            style: Style {
                height: Val::Percent(100.),
                width: Val::Percent(100.),
                display: Display::Flex,
                flex_direction: FlexDirection::Column,
                position_type: PositionType::Absolute,
                left: Val::Percent(0.0),
                padding: UiRect::all(Val::Px(8.)),
                ..default()
            },
            background_color: BackgroundColor(Srgba::hex("#30201c").unwrap().into()),
            ..default()
        },
        MapDisplayer
    ))
        .with_children(|parent| {
            println!("display choices, with effect ? {:?}", request.effect);
            println!("display choices {:?}", request.next);
            if request.next.len() == 0 {
                println!("request.id == \"quest\" ? {}", request.id == "quest");
                if request.quest.is_some() {
                    display_quest_list(quests, player, inventory, parent);
                }
                create_response_button(parent, "=> fin", DialogButtonAction::End { value: request.action.clone(), effect: request.effect.to_string() });
            } else {
                request.next.iter().for_each(|item| {
                    println!("item.as_str() == \"quest\" ? {}", item.as_str() == "quest");
                    match dialog.player.get(item.as_str()) {
                        Some(response) => {
                            println!("display player id {} text {} cond {:?}, {:?}", response.id, response.text, response.condition, response.condition.is_valid(player, inventory));
                            if response.condition.is_valid(player, inventory) {
                                create_response_button(parent, &response.text, DialogButtonAction::Response { value: response.id.to_string(), effect: request.effect.to_string() });
                            };
                        }
                        _ => {
                            create_response_button(parent, "=> suite", DialogButtonAction::Response { value: item.to_string(), effect: request.effect.to_string() });
                        }
                    }
                })
            }
        });
}

fn display_quest_list(quests: &Quests, player: &Player, inventory: &Inventory, parent: &mut ChildBuilder) {
    println!("meta {:?}", quests.meta);
    let mut locations_quests = quests.all.iter().filter(|(_k, quest)| {
        println!("quest meta {:?} for {}, res {}", quests.meta.get(&quest.pnj), player.location, quests.meta.get(&quest.pnj).map(|meta| meta.location == player.location).unwrap_or(false));
        quests.meta.get(&quest.pnj).map(|meta| meta.location == player.location).unwrap_or(false)
    })
        .map(|(_, q)| {
            q.clone()
        })
        .collect::<Vec<Quest>>();
    locations_quests.sort_by(| a,b|{
        a.priority.cmp(&b.priority)
    });
    for quest in locations_quests {
        let qname = quest.name.to_string();
        let qid = quest.id.to_string();
        println!("quest status for {} is {:?}", qid, player.have_done(&qid));
        if player.have_done(&qid) {
            create_done_quest_button(parent, &qname);
        } else if quest.cond.is_valid(player, inventory) {
            create_response_button(parent, &qname, DialogButtonAction::Quest { value: quest });
        } else {
            create_unknow_quest_button(parent);
        }
    }
}

fn create_response_button(parent: &mut ChildBuilder, text: &str, action: DialogButtonAction) {
    parent
        .spawn((
            ButtonBundle {
                style: Style {
                    left: Val::Px(10.),
                    top: Val::Px(5.),
                    height: Val::Px(30.),
                    width: Val::Percent(100.),
                    ..default()
                },
                ..default()
            },
            action
        ))
        .with_children(|parent| {
            parent.spawn((
                NodeBundle {
                    style: Style {
                        position_type: PositionType::Absolute,
                        left: Val::Px(10.),
                        top: Val::Px(5.),
                        height: Val::Px(10.),
                        width: Val::Px(10.),
                        ..default()
                    },
                    ..default()
                }/*,
                UiImage::new(handle)*/
            ));
            parent.spawn((
                TextBundle::from_section(
                    text,
                    TextStyle {
                        font_size: 15.0,
                        color: ANTIQUE_WHITE.into(),
                        ..default()
                    },
                ), MenuButton
            ));
        });
}

fn create_done_quest_button(parent: &mut ChildBuilder, text: &str) {
    parent
        .spawn((
            ButtonBundle {
                style: Style {
                    left: Val::Px(10.),
                    top: Val::Px(5.),
                    height: Val::Px(30.),
                    width: Val::Percent(100.),
                    ..default()
                },
                ..default()
            },
        ))
        .with_children(|parent| {
            parent.spawn((
                NodeBundle {
                    style: Style {
                        position_type: PositionType::Absolute,
                        left: Val::Px(10.),
                        top: Val::Px(5.),
                        height: Val::Px(10.),
                        width: Val::Px(10.),
                        ..default()
                    },
                    ..default()
                }/*,
                UiImage::new(handle)*/
            ));
            parent.spawn((
                TextBundle::from_section(
                    text,
                    TextStyle {
                        font_size: 15.0,
                        color: GREEN.into(),
                        ..default()
                    },
                ), MenuButton
            ));
        });
}

fn create_unknow_quest_button(parent: &mut ChildBuilder) {
    parent
        .spawn((
            NodeBundle {
                style: Style {
                    left: Val::Px(10.),
                    top: Val::Px(5.),
                    height: Val::Px(30.),
                    width: Val::Percent(100.),
                    ..default()
                },
                ..default()
            },
        ))
        .with_children(|parent| {
            parent.spawn((
                NodeBundle {
                    style: Style {
                        position_type: PositionType::Absolute,
                        left: Val::Px(10.),
                        top: Val::Px(5.),
                        height: Val::Px(10.),
                        width: Val::Px(10.),
                        ..default()
                    },
                    ..default()
                }/*,
                UiImage::new(handle)*/
            ));
            parent.spawn((
                TextBundle::from_section(
                    "? ? ? ?",
                    TextStyle {
                        font_size: 15.0,
                        color: Color::srgb(0.2,0.2,0.2).into(),
                        ..default()
                    },
                ), MenuButton
            ));
        });
}
