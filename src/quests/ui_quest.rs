use bevy::prelude::*;
use crate::common::inventory::Inventory;
use crate::common::player::Player;
use crate::common_ui_sprite::{AvatarColumnUI, CustomisablePart, WorldSprite};
use crate::quests::action::DialogButtonAction;
use crate::quests::quests::{QuestStep, Quests};

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
pub enum QuestState {
    #[default]
    Loading,
    Customize,
    Ready,
}


#[derive(Component)]
enum MenuButtonAction {
    Quit,
}

#[derive(Component)]
struct QuestUIItem;

const BUTTON_TEXT_COLOR: Color = Color::srgb(0.1, 0.1, 0.1);
const TEXT_COLOR: Color = Color::srgb(0.9, 0.9, 0.9);


pub fn create_quest_button(
    mut commands: Commands,
    ui_root: Query<Entity, With<AvatarColumnUI>>,
    common_sprites: Res<WorldSprite>,
    player: Res<Player>,
    quests: Res<Quests>,
)
{
    if let Ok(entity) = ui_root.get_single() {
        println!("°°°°°°°°°°°°°°°°°° Quest Action ");
        println!("all quests {:?} id {:?}", quests.all.len(), quests.all.iter().map(|(k, _)| k.clone()).collect::<Vec<_>>());
        println!("player.active_quest {:?}", player.active_quest);
        if let Some((quest, _)) = &player.active_quest{
            let button_style = Style {
                width: Val::Percent(100.0),
                margin: UiRect::all(Val::Px(10.0)),
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                position_type: PositionType::Absolute,
                top: Val::Px(300.0),
                ..default()
            };
            commands
                .entity(entity)
                .with_children(|parent| {
                    parent
                        .spawn((
                            ButtonBundle {
                                style: button_style.clone(),
                                image: UiImage::from(common_sprites.buttons.button_next.clone()),
                                ..default()
                            },
                            DialogButtonAction::ListQuest
                        ))
                        .with_children(|parent| {
                            parent
                                .spawn(NodeBundle {
                                    style: Style {
                                        margin: UiRect::new(
                                            Val::Px(10.0),
                                            Val::Px(10.0),
                                            Val::Px(20.0),
                                            Val::Px(20.0),
                                        ),
                                        width: Val::Percent(100.0),
                                        height: Val::Percent(100.0),
                                        ..default()
                                    },
                                    ..default()
                                })
                                .with_children(|parent| {
                                    parent.spawn((
                                        TextBundle::from_section(
                                            quest.name.clone(),
                                            TextStyle {
                                                font_size: 14.0,
                                                color: BUTTON_TEXT_COLOR,
                                                ..default()
                                            },
                                        )
                                    ));
                                });
                        });
                });
        }
    }
}

pub fn load_quest_page_interface(
    mut commands: Commands,
    mut world_state: ResMut<NextState<QuestState>>,
    ui_root: Query<Entity, With<CustomisablePart>>,
    sprites: Res<WorldSprite>,
    mut player: ResMut<Player>,
)
{
    println!("customize quest page");
    let entity = ui_root.get_single().expect("Common UI fail to load ");
    commands
        .entity(entity)
        .with_children(|parent| {
            parent.spawn(NodeBundle {
                style: Style {
                    width: Val::Percent(100.),
                    flex_direction: FlexDirection::Column,
                    margin: UiRect::top(Val::Px(100.)),
                    ..default()
                },
                ..default()
            })
                .with_children(|parent| {
                    display_actives_quests(parent, &sprites, &player);
                    create_menu(parent, &sprites, &player.location);
                });
        });
    world_state.set(QuestState::Ready);
}

fn display_actives_quests(mut parent: &mut ChildBuilder, common_sprites: &WorldSprite, player: &Player)
{
    if let Some(( quest, current_step)) = &player.active_quest {
        parent.
            spawn((
                NodeBundle {
                    style: Style {
                        left: Val::Px(100.),
                        width: Val::Px(500.),
                        display: Display::Flex,
                        flex_direction: FlexDirection::Column,
                        ..default()
                    },
                    ..default()
                },
                QuestUIItem
            ))
            .with_children(|parent| {
                parent.spawn(
                    TextBundle::from_section(
                        quest.name.to_string(),
                        TextStyle {
                            font_size: 20.0,
                            color: BUTTON_TEXT_COLOR,
                            ..default()
                        },
                    )
                );
                println!("quest.descr {:?}", quest.descr);
                parent.spawn(
                    TextBundle::from_section(
                        quest.descr.to_string(),
                        TextStyle {
                            font_size: 12.0,
                            color: Color::WHITE,
                            ..default()
                        },
                    )
                );
                let mut keep = true;
                println!("fucnink quest {:?}", quest);
                quest.steps
                    .iter()
                    .enumerate()
                    .filter(|(_, q_step)|{
                        match q_step {
                            QuestStep::End(_) => {
                                keep = false;
                                true
                            }
                            _ => {
                                keep
                            }
                        }
                    })
                    .for_each(|(index, q_step)| {
                        println!("Quest step to dusplay {:?} {:?}",index, q_step);
                    let text = match q_step {
                        QuestStep::Talk(t) => { "Parlez à ".to_string() + &t.dialog}
                        QuestStep::Create(c) => {c.title.clone()}
                        QuestStep::Use(u) => {u.text.to_string()}
                        QuestStep::Cauldron(c) => { c.title.to_string()}
                        QuestStep::Gold(_) => {"".to_string()}
                        QuestStep::XP(_) => {"".to_string()}
                        QuestStep::Reput(_, _) => {"".to_string()}
                        QuestStep::Element(_) => {"".to_string()}
                        QuestStep::Item(_) => {"".to_string()}
                        QuestStep::PlayMod(_) => {"".to_string()}
                        QuestStep::End(e) => {"Retourner à ".to_string() + &e.place }
                        QuestStep::Msg(m) => {m.text.to_string()}
                    };
                        let final_text= if index < (current_step -1){
                            " V ".to_string() + &text
                        } else {
                            " X ".to_string() + &text
                        };
                        let final_color= if index < (current_step -1){
                            Color::hsl(0.44, 1.00, 0.33)
                        } else {
                            Color::WHITE
                        };
                    parent.spawn(
                        TextBundle::from_section(
                            final_text,
                            TextStyle {
                                font_size: 12.0,
                                color: final_color,
                                ..default()
                            },
                        )
                    );
                })

            });
    }
}

fn create_menu(parent: &mut ChildBuilder, common_sprites: &WorldSprite, location: &str) {
    let button_style = Style {
        width: Val::Px(200.0),
        height: Val::Px(30.0),
        margin: UiRect::all(Val::Px(20.0)),
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        ..default()
    };
    parent
        .spawn((NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Row,
                ..default()
            },
            ..default()
        }))
        .with_children(|parent| {
            parent
                .spawn((
                    ButtonBundle {
                        style: button_style.clone(),
                        image: UiImage::from(common_sprites.buttons.button_next.clone()),
                        ..default()
                    },
                    DialogButtonAction::ReturnWorld
                ))
                .with_children(|parent| {
                    parent.spawn((
                        TextBundle::from_section(
                            "Revenir à ".to_string() + location,
                            TextStyle {
                                font_size: 20.0,
                                color: BUTTON_TEXT_COLOR,
                                ..default()
                            },
                        )
                    ));
                });
        });
}
