use std::cmp;
use bevy::utils::HashMap;
use bevy::prelude::*;

use crate::common::element::Element;
use crate::places::model::Align;
use crate::quests::quests::{Quest, QuestStep};

#[derive(Resource)]
pub struct Player {
    pub id: u64,
    pub first_play_done: bool,
    pub active_quest: Option<(Quest, usize)>, // (id, step)
    pub quest_status: HashMap<String, bool>,
    pub reput: HashMap<Align, i8>,
    pub school: Align,
    pub effects: Vec<String>,
    pub public_alias: String,
    pub level: u8,
    pub location: String,
}

impl Player {
    pub fn doing(&self, id: &String) -> bool {
        if let Some((quest, _)) = &self.active_quest {
            &quest.name == id
        } else {
            false
        }
    }

    pub fn have_done(&self, id: &String) -> bool {
        println!("quest status {:?}", self.quest_status);
        self.quest_status.get(id).map(|b| *b).unwrap_or(false)
    }

    pub fn have_reput(&self, school: &Align, value: i8) -> bool {
        self.get_reput(school) >= value
    }

    pub fn get_reput(&self, school: &Align) -> i8 {
        self.reput.get(school).map_or(0, |c| *c)
    }

    pub fn have_effect(&self, id: &String) -> bool {
        self.effects.iter().find(|fx| *fx == id).is_some()
    }

    /**
    * If player must create element, decrease element to create and pass to next if all are create.
    * else, do nothing
    */
    pub fn have_create_elements(&mut self, loots: &HashMap<Element, u8>) {
        println!("have_create_elements");
        if let Some((quest, step)) = &mut self.active_quest {
            match quest.get_step_mut(*step) {
                Some(QuestStep::Create(create)) => {
                    create.elements = create.elements.iter()
                        .map(|(element, count)| {
                            if let Some(nb) = loots.get(element) {
                                (element.to_owned(), cmp::max(0, count - nb))
                            } else {
                                (element.to_owned(), count.to_owned())
                            }
                        })
                        .filter(|(element, count)| count > &0)
                        .collect();
                    println!("have_create_elements at end {:?}", create.elements);
                    if create.elements.len() == 0 {
                        println!("incr quest");
                        self.incr_step();
                        println!("{:?}",self.active_quest)
                    }
                }
                _ => {}
            }
        }
    }

    pub fn incr_step(&mut self) {
        println!("incr step");
        self.active_quest.as_mut().unwrap().1 += 1;
    }
}