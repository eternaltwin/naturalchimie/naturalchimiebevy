use std::str::from_utf8;
use bevy::math::Vec2;
use quick_xml::events::attributes::Attribute;
use crate::common::element::Element;
use crate::common::item::PlayableItem;
use crate::places::model::Align;
use crate::quests::QObjet::{QObject, QuestObject};

pub struct helpers {}

impl helpers {
    pub fn get_value(attr: &Attribute) -> String {
        from_utf8(attr.value.as_ref()).unwrap_or("").to_string()
    }

    pub fn get_value_as_str<'a>(attr: &'a Attribute) -> &'a str {
        from_utf8(attr.value.as_ref()).unwrap_or("")
    }

    pub fn get_elements<'a>(attr: &'a Attribute) -> Vec<(Element, u8)> {
        // "Elt(7):5;Elt(16):2;Elt(12):2"
        let str = helpers::get_value_as_str(attr);
        let mut res = Vec::new();
        str.split(";")
            .for_each(|once_el| {
                let el = Self::extract_element_from_str(once_el);
                res.push(el);
            });
        res
    }

    pub fn get_quests_objects<'a>(attr: &'a Attribute) -> Vec<QObject> {
        // "Elt(5):30;Grenade(1):1;QuestObj(geminishred):6;CountBlock(1):80"
        let str = helpers::get_value_as_str(attr);
        let mut res = Vec::new();
        str.split(";")
            .for_each(|once_el| {
                let el = if once_el.starts_with("Elt") {
                    let r = Self::extract_element_from_str(once_el);
                    QObject::Elt(r.0, r.1)
                } else if once_el.starts_with("QuestObj") {
                    let r = Self::extract_quest_obj_from_str(once_el);
                    QObject::Quest(r.0, r.1)
                } else {
                    let r = Self::extract_item_from_str(once_el);
                    QObject::Item(r.0, r.1)
                };
                res.push(el);
            });
        res
    }

    pub fn extract_element_from_str(input: &str) -> (Element, u8) {
        let parts = input.split(":").collect::<Vec<&str>>();
        let el_idx = parts[0][4..parts[0].len() - 1].parse::<i32>().unwrap();
        let count = parts[1].parse().unwrap_or(0);
        (Element::from_i32(el_idx), count)
    }

    pub fn extract_item_from_str(input: &str) -> (PlayableItem, u8) {
        let parts = input.split(":").collect::<Vec<&str>>();
        let item = match parts[0] {
            "Pistonide" => { PlayableItem::Pistonide }
            "PolarBomb" => { PlayableItem::PolarBomb }
            "PearGrain(0)" => { PlayableItem::PearGrain }
            "PearGrain(1)" => { PlayableItem::PearTreeStump }
            "Grenade(0)" => { PlayableItem::Charcleur }
            "Grenade(1)" => { PlayableItem::DelayedCharcleur }
            "Delorean(0)" => { PlayableItem::Delorean }
            "Dynamit(0)" => { PlayableItem::HorizontalDynamit }
            "Dynamit(1)" => { PlayableItem::VerticalDynamit }
            "Dynamit(2)" => { PlayableItem::CrossedDynamit }
            "Pa" => { PlayableItem::Pa }
            "MentorHand" => { PlayableItem::MentorHand }
            "Patchinko" => { PlayableItem::Patchinko }
            _ => {
                println!("item not Parsed {:?}", parts[0]);
                PlayableItem::Unparsed(parts[0].to_string())
            }
        };
        let count = parts[1].parse().unwrap_or(0);
        (item, count)
    }

    pub fn extract_quest_obj_from_str(input: &str) -> (QuestObject, u8) {
        let parts = input.split(":").collect::<Vec<&str>>();
        let item = match parts[0] {
            "chocapic" => { QuestObject::Chocapic }
            "geminishred" => { QuestObject::Geminishred }
            "geminishregular" => { QuestObject::Geminishregular }
            "ink" => { QuestObject::Ink }
            "Wombat" => { QuestObject::Wombat }
            "Teleport" => { QuestObject::Teleport }
            "GodFather" => { QuestObject::GodFather }
            "ocarina" => { QuestObject::Ocarina }
            "broom2" => { QuestObject::Broom2 }
            "oblForm" => { QuestObject::OblForm }
            "livre" => { QuestObject::Livre }
            "cage" => { QuestObject::Cage }
            "goldTrap" => { QuestObject::GoldTrap }
            "corb" => { QuestObject::Corb }
            "fiente" => { QuestObject::Fiente }
            "michelin" => { QuestObject::Michelin }
            _ => {
                println!("item not Parsed {:?}", parts[0]);
                QuestObject::Unparsed(parts[0].to_string())
            }
        };
        let count = if parts.len() == 1 {
            1
        } else {
            parts[1].parse().unwrap_or(0)
        };
        (item, count)
    }

    pub fn get_numerical_value(attr: &Attribute) -> i32 {
        from_utf8(attr.value.as_ref()).unwrap_or("0").parse().unwrap_or(0)
    }

    pub fn get_boolean_value(attr: &Attribute) -> bool {
        helpers::get_value(attr) == "1"
    }

    pub fn get_vector_value(attr: &Attribute) -> Vec2 {
        match from_utf8(attr.value.as_ref()).unwrap_or("0:0").split_once(":") {
            Some((x, y)) => {
                Vec2::new(x.parse().unwrap_or(0.), y.parse().unwrap_or(0.))
            }
            _ => { Vec2::new(0., 0.) }
        }
    }

    pub fn get_vec_vector(attr: &Attribute) -> Vec<Vec2> {
        from_utf8(attr.value.as_ref()).unwrap_or("0:0").split(",").map(|point| {
            match point.split_once(":") {
                Some((x, y)) => { Vec2::new(x.parse().unwrap_or(0.), y.parse().unwrap_or(0.)) }
                _ => { Vec2::new(0., 0.) }
            }
        })
            .collect()
    }

    pub fn get_align(attr: &Attribute) -> Align {
        match attr.value.as_ref() {
            b"gui" => Align::Guilde,
            b"ap" => Align::Audepint,
            b"jz" => Align::Jeezara,
            b"gm" => Align::Gemini,
            b"sk" => Align::ShangKah,
            _ => Align::Guilde
        }
    }

    pub fn get_align_from_string(attr: &str) -> Align {
        match attr {
            "gui" => Align::Guilde,
            "ap" => Align::Audepint,
            "jz" => Align::Jeezara,
            "gm" => Align::Gemini,
            "sk" => Align::ShangKah,
            _ => Align::None
        }
    }
}