use bevy::prelude::Resource;
use crate::common::element::Element;

#[derive(Resource)]
pub struct Inventory {
    pub elements: [u32;12],
    pub items: [u32;12],
    pub kubor: u32,
}

impl Inventory {
    pub fn add_element(&mut self, element: &Element, count: u32) {
        self.add_index(element.as_index(),count)
    }
    pub fn add_index(&mut self, element: usize, count: u32) {
        self.elements[element] = count;
    }
}