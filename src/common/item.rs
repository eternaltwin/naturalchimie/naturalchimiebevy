use bevy::prelude::*;

pub struct ItemsSheets {
    in_game_playable: [Handle<Image>; 0],
    in_game_not_playable: [Handle<Image>; 0],
    other: [Handle<Image>; 0],
}


impl ItemsSheets {
    pub fn new(_assets: &AssetServer) -> ItemsSheets {
        ItemsSheets{
            in_game_playable: [],
            in_game_not_playable: [],
            other: [],
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum PlayableItem {
    Unparsed(String),
    Pistonide,
    PolarBomb,
    Charcleur,
    DelayedCharcleur,
    Delorean,
    HorizontalDynamit,
    CrossedDynamit,
    VerticalDynamit,
    PearTreeStump,
    PearGrain,
    Patchinko,
    MentorHand,
    Pa,
}