use std::str::Utf8Error;
use bevy::{
    asset::{io::Reader, AssetLoader, LoadContext},
    prelude::*,
    reflect::TypePath,
    utils::BoxedFuture,
};
use bevy::asset::io::AsyncReadExt;
use thiserror;
use serde::Deserialize;

#[derive(Asset, TypePath, Debug, Deserialize)]
pub struct XmlAsset(pub String);

/// Possible errors that can be produced by [`CustomAssetLoader`]
#[non_exhaustive]
#[derive(Debug, thiserror::Error)]
pub enum XmlAssetLoaderError {
    /// An [IO](std::io) Error
    #[error("Could load Toml: {0}")]
    Io(#[from] std::io::Error),
    #[error("Could parse Toml: {0}")]
    Encode(#[from] Utf8Error),
}

#[derive(Default)]
pub struct XmlAssetLoader;

impl AssetLoader for XmlAssetLoader {
    type Asset = XmlAsset;
    type Settings = ();
    type Error = XmlAssetLoaderError;
    fn load<'a>(
        &'a self,
        reader: &'a mut Reader,
        _settings: &'a (),
        _load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<Self::Asset, Self::Error>> {
        Box::pin(async move {
            let mut bytes = Vec::new();
            reader.read_to_end(&mut bytes).await?;
            let data_str = std::str::from_utf8(&bytes)?;
            Ok(XmlAsset(data_str.to_string()))
        })
    }

    fn extensions(&self) -> &[&str] {
        &["xml"]
    }
}