use bevy::prelude::Resource;
use crate::common::item::PlayableItem;

#[derive(Resource)]
pub struct Belt {
    pub cases: [BeltCase; 4]
}

pub enum BeltCase {
    Blocked,
    Empty,
    Item(PlayableItem)
}
