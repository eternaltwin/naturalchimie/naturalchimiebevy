use std::fmt;
use bevy::prelude::*;
use serde::{Deserialize, Serialize};

pub struct ElementsSheets {
    pub(crate) handles: [Handle<Image>; 30],
}


impl ElementsSheets {
    pub fn new(assets: &AssetServer) -> ElementsSheets {
        ElementsSheets {
            handles: [
                assets.load("images/elements/elt.png"),
                assets.load("images/elements/elt1.png"),
                assets.load("images/elements/elt2.png"),
                assets.load("images/elements/elt3.png"),
                assets.load("images/elements/elt4.png"),
                assets.load("images/elements/elt5.png"),
                assets.load("images/elements/elt6.png"),
                assets.load("images/elements/elt7.png"),
                assets.load("images/elements/elt8.png"),
                assets.load("images/elements/elt9.png"),
                assets.load("images/elements/elt10.png"),
                assets.load("images/elements/elt11.png"),
                assets.load("images/elements/elt12.png"),
                assets.load("images/elements/elt13.png"),
                assets.load("images/elements/elt14.png"),
                assets.load("images/elements/elt15.png"),
                assets.load("images/elements/elt16.png"),
                assets.load("images/elements/elt17.png"),
                assets.load("images/elements/elt18.png"),
                assets.load("images/elements/elt19.png"),
                assets.load("images/elements/elt20.png"),
                assets.load("images/elements/elt21.png"),
                assets.load("images/elements/elt22.png"),
                assets.load("images/elements/elt23.png"),
                assets.load("images/elements/elt24.png"),
                assets.load("images/elements/elt25.png"),
                assets.load("images/elements/elt26.png"),
                assets.load("images/elements/elt27.png"),
                assets.load("images/elements/elt28.png"),
                assets.load("images/elements/unkown.png"),
            ]
        }
    }
}

//keep the element as an enumeration to enforce the type and avoid confusion with objects or anything else that can be modeled as a number
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug, Default, Reflect, Serialize, Deserialize)]
#[reflect()]
pub enum Element {
    #[default]
    GreenPotion,
    YellowPotion,
    RedPotion,
    PurplePotion,
    MintLeaf,
    MadEye,
    Cranos,
    Flaum,
    Oxide,
    Copper,
    Mercury,
    GoldNugget,
}

impl Element {
    pub fn from_i32(value: i32) -> Element {
        match value {
            0 => Element::GreenPotion,
            1 => Element::YellowPotion,
            2 => Element::RedPotion,
            3 => Element::PurplePotion,
            4 => Element::MintLeaf,
            5 => Element::MadEye,
            6 => Element::Cranos,
            7 => Element::Flaum,
            8 => Element::Oxide,
            9 => Element::Copper,
            10 => Element::Mercury,
            11 => Element::GoldNugget,
            _ => Element::GreenPotion,
        }
    }

    pub fn next(&self) -> Element {
        match self {
            Element::GreenPotion => { Element::YellowPotion }
            Element::YellowPotion => { Element::RedPotion }
            Element::RedPotion => { Element::PurplePotion }
            Element::PurplePotion => { Element::MintLeaf }
            Element::MintLeaf => { Element::MadEye }
            Element::MadEye => { Element::Cranos }
            Element::Cranos => { Element::Flaum }
            Element::Flaum => { Element::Oxide }
            Element::Oxide => { Element::Copper }
            Element::Copper => { Element::Mercury }
            Element::Mercury => { Element::GoldNugget }
            Element::GoldNugget => { Element::GoldNugget }
        }
    }

    pub fn as_index<'a>(&self) -> usize {
        match self {
            Element::GreenPotion => { 0 }
            Element::YellowPotion => { 1 }
            Element::RedPotion => { 2 }
            Element::PurplePotion => { 3 }
            Element::MintLeaf => { 4 }
            Element::MadEye => { 5 }
            Element::Cranos => { 6 }
            Element::Flaum => { 7 }
            Element::Oxide => { 8 }
            Element::Copper => { 9 }
            Element::Mercury => { 10 }
            Element::GoldNugget => { 11 }
        }
    }
    pub fn get_texture<'a>(&self, sprites: &'a ElementsSheets) -> &'a Handle<Image> {
        &sprites.handles[self.as_index()]
    }
}

impl fmt::Display for Element {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Element::GreenPotion => { write!(f, "Element::One") }
            Element::YellowPotion => { write!(f, "Element::Two") }
            Element::RedPotion => { write!(f, "Element::Three") }
            Element::PurplePotion => { write!(f, "Element::Four") }
            Element::MintLeaf => { write!(f, "Element::Five") }
            Element::MadEye => { write!(f, "Element::Six") }
            Element::Cranos => { write!(f, "Element::Seven") }
            Element::Flaum => { write!(f, "Element::Height") }
            Element::Oxide => { write!(f, "Element::Nine") }
            Element::Copper => { write!(f, "Element::Ten") }
            Element::Mercury => { write!(f, "Element::Eleven") }
            Element::GoldNugget => { write!(f, "Element::Twelve") }
        }
    }
}