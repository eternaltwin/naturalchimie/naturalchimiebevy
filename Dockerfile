FROM nginx

RUN rm /etc/nginx/conf.d/default.conf
COPY public /www/public
COPY assets /www/assets/
COPY assets/config.toml /www/assets/
COPY index.html /www
COPY build/nginx /etc/nginx/conf.d