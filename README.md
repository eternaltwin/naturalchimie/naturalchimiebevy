# NaturalchimieBevy



## Description

Remake of Naturalchimie 2 in Rust based on [Bevy engine](https://bevyengine.org/).
Assets are extract from [WebGamesArchives    Motion Twin](https://github.com/motion-twin/WebGamesArchives)

## Build and run 

```shell
#build
 cargo build --release --target wasm32-unknown-unknown &&  wasm-bindgen --out-dir ./public/ --target web ./target/wasm32-unknown-unknown/release/NaturalchimieBevy.wasm &&  docker build -t naturalchimiebevy:latest .

#run 
docker run -p <port>:80  --name NaturalchimieBevy docker.io/library/naturalchimiebevy:latest
```

You can also test in local with by run a Bevy app : 
```shell
cargo run 
```

## link between naming and object

PearGrain(0) => graine de poirier
PearGrain(1) => souche de poirier

grenade(0) => Charcleur
grenade(1) => Charcleur à retardement

Dynamit(0) =>  dynamite horizontal ==
Dynamit(1) =>  dynamite vertical ||
Dynamit(2) =>  dynamite croisée =||=

Delorean(0) => réglisse de Delorean ??

## Contributing

Feel you free to contribute. Open an issue or take one  to show other on what you work.


## Dev documentation 

[board logic](src/board/Readme.md)